# Project MeFit

### Description

This is our groups development of the web application MeFit, a case ordered by Experis Academy.
The project repo is divided into two parts: The frontend made with React & Bootstrap and the backend with Java and PostgresDB. 
The backend can be found from the root folder: 
``` 
src/main/java
```

The frontend can be found in the from the root folder
``` 
src/frontend
```

### Dependencies

* The backend uses Gradle to build the project, do not change as this might cause issues to run the database connection.
* The frontend runs with the help of nodejs, required to get the projet working. 

### Installing

* To install the entire web application, first, simply clone the repo below:
```
git clone https://gitlab.com/Jacob_A/mefit_case_c_malmberg_j_andersson_p_hjelmberg.git
```

* To install the dependencies for the frontend, run the following command in the previously mentioned folder: src/frontend
```
npm install
``` 

* The backed itself does not require any installing. However, in order to run the program locally a DB connection needs to be added to the application.properties file.

### Usage

How to run the frontend part of the application, run the command:
```
npm start
```
This will run the NodeJS service on port 3000 by default.

To run the connection to the backend of the application, do the following:
* Open IntelliJ
* Run or use Shift+F10 on the 'Assignment6WebApiAndSpringApplication'

#### Note: As previously mentioned, in order to run the program locally a DB connection needs to be added to the application.properties file


### Endpoints (OpenAPI)

To see the endpoints and its method, paths, accepted parameters etc. Run the backend and go to 
```
http://localhost:8080/swagger-ui/index.html#/
```

### Contributors

Contributors and contact information

Christoffer Malmberg
[@ChristofferMalmberg](https://gitlab.com/ChristofferMalmberg)

Jacob Andersson
[@Jacob_A](https://gitlab.com/Jacob_A/)

Philip Hjelmberg
[@PhilipHjelmberg](https://gitlab.com/PhilipHjelmberg)

### Contributing

PRs are not accepted
