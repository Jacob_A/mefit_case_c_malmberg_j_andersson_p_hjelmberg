--Program/workout/sets/exercises
INSERT INTO program (name, category) VALUES ('super program', 'cardio');
INSERT INTO program (name, category) VALUES ('super workout', 'sweaty');
INSERT INTO exercise (name,repetitions, description, target_muscle_group, image_link, video_link) VALUES ('squats',3, 'get low', 'legs', null, null);
INSERT INTO exercise (name, repetitions, description, target_muscle_group) VALUES ('Pushups',5, 'Giga strong', 'Triceps, etc');

INSERT INTO workout (name, type, complete) VALUES ('running man','cardio', false);
INSERT INTO workout (name, type, complete) VALUES ('Arms and stamina','mixed workout', false);
INSERT INTO workout_exercise (exercise_id, workout_id) VALUES (1,1);
INSERT INTO workout_exercise (exercise_id, workout_id) VALUES (1,2);
INSERT INTO workout_exercise (exercise_id, workout_id) VALUES (2,2);

INSERT INTO program_workout (program_id, workout_id) VALUES (1,1);
INSERT INTO program_workout (program_id, workout_id) VALUES (2,1);
INSERT INTO program_workout (program_id, workout_id) VALUES (2,2);

--User
INSERT INTO users (id,first_name, last_name, contributor, admin) VALUES ('fecf5684-cdca-4899-9e55-6929230addff','Testy', 'Tester', false, false);
INSERT INTO address (address_line1, postal_code, city, country) VALUES ('Tester Street 12',12345,'Tester city', 'Tester land');
INSERT INTO profile (user_id, address_id, weight, height) VALUES ('fecf5684-cdca-4899-9e55-6929230addff',1,12,13);

INSERT INTO users (id,first_name, last_name, contributor, admin) VALUES ('sdfafaqweroi234asd','Testy jr', 'Tester', false, false);
INSERT INTO profile (user_id, address_id, weight, height) VALUES ('sdfafaqweroi234asd',1,7,12);

--Admin
INSERT INTO users (id,first_name, last_name, contributor, admin) VALUES ('megaadmin1231231sdf','Admin', 'Adminer', true, true);
INSERT INTO address (address_line1, postal_code, city, country) VALUES ('Admin Street 12',735734,'admin city', 'admin land');
INSERT INTO profile (user_id, address_id, weight, height) VALUES ('megaadmin1231231sdf',2, 75, 210);

--Goals
INSERT INTO goal (name ,achieved, profile_id,end_date) VALUES ('Giga chad',false, 1, '2023-01-01');
INSERT INTO goal (name ,achieved, profile_id,end_date) VALUES ('Mega arms',true, 1, '2022-12-24');
INSERT INTO goal (name, achieved, profile_id,end_date) VALUES ('Track star',false, 2, '2023-01-01');
INSERT INTO goal_workout (goal_id, workout_id) VALUES (1,1);
INSERT INTO goal_workout (goal_id, workout_id) VALUES (1,2);
INSERT INTO goal_workout (goal_id, workout_id) VALUES (2,2);
INSERT INTO goal_workout (goal_id, workout_id) VALUES (3,1);
