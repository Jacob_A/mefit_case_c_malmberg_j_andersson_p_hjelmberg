package com.example.mefit_case.service.program;

import com.example.mefit_case.model.Program;
import com.example.mefit_case.service.CrudService;

public interface ProgramService extends CrudService<Program,Integer> {
    boolean programExists(int id);
}
