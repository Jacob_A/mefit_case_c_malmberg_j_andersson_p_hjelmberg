package com.example.mefit_case.service.program;

import com.example.mefit_case.exceptions.MalformedRequestException;
import com.example.mefit_case.exceptions.ProgramNotFoundException;
import com.example.mefit_case.model.Program;
import com.example.mefit_case.repository.ProgramRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ProgramServiceImpl implements ProgramService {
    private final ProgramRepository programRepository;

    public ProgramServiceImpl(ProgramRepository programRepository) {
        this.programRepository = programRepository;
    }


    @Override
    public Program findById(Integer id) {
        if(id < 1){
            return null;
        }
        return this.programRepository.findById(id).orElseThrow(() -> new ProgramNotFoundException(id));
    }

    @Override
    public Collection<Program> findAll() {
        return programRepository.findAll();
    }

    @Override
    public Program add(Program entity) {
        try{
            return programRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public Program update(Program entity) {
        try{
            return programRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public void deleteById(Integer id) {
        Program program = programRepository.findById(id).orElseThrow(() -> new ProgramNotFoundException(id));
        program.getGoals().forEach(goal -> goal.setProgram(null));
        programRepository.deleteById(id);
    }

    @Override
    public boolean programExists(int id) {
        return programRepository.existsById(id);
    }
}
