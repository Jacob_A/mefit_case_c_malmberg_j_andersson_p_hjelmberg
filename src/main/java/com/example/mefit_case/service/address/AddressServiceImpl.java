package com.example.mefit_case.service.address;

import com.example.mefit_case.exceptions.AddressNotFoundException;
import com.example.mefit_case.exceptions.MalformedRequestException;
import com.example.mefit_case.model.Address;
import com.example.mefit_case.repository.AddressRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AddressServiceImpl implements AddressService {
   private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }


    @Override
    public Address findById(Integer id) {
        if(id < 1){
            return null;
        }
        return this.addressRepository.findById(id).orElseThrow(() -> new AddressNotFoundException(id));
    }

    @Override
    public Collection<Address> findAll() {
        return addressRepository.findAll();
    }

    @Override
    public Address add(Address entity) {
        try {
            return addressRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public Address update(Address entity) {
        try {
            return addressRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public void deleteById(Integer id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new AddressNotFoundException(id));
        address.getProfiles().forEach(profile -> profile.setAddress(null));
        addressRepository.deleteById(id);
    }

    @Override
    public boolean addressExists(int id) {
        return addressRepository.existsById(id);
    }
}
