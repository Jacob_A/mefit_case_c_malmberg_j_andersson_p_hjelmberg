package com.example.mefit_case.service.address;

import com.example.mefit_case.model.Address;
import com.example.mefit_case.service.CrudService;

public interface AddressService extends CrudService<Address,Integer> {
    boolean addressExists(int id);
}
