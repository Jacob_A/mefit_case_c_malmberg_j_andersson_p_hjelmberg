package com.example.mefit_case.service.workout;

import com.example.mefit_case.exceptions.MalformedRequestException;
import com.example.mefit_case.exceptions.WorkoutNotFoundException;
import com.example.mefit_case.model.Workout;
import com.example.mefit_case.repository.WorkoutRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class WorkoutServiceImpl implements WorkoutService {
    private final WorkoutRepository workoutRepository;

    public WorkoutServiceImpl(WorkoutRepository workoutRepository) {
        this.workoutRepository = workoutRepository;
    }

    @Override
    public Workout findById(Integer id) {
        if(id < 1){
            return null;
        }
        return this.workoutRepository.findById(id).orElseThrow(() -> new WorkoutNotFoundException(id));
    }

    @Override
    public Collection<Workout> findAll() {
        return workoutRepository.findAll();
    }

    @Override
    public Workout add(Workout entity) {
        try{
            return workoutRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public Workout update(Workout entity) {
        try{
            return workoutRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public void deleteById(Integer id) {
        workoutRepository.deleteFromGoal_workout(id);
        workoutRepository.deleteFromProgram_workout(id);
        workoutRepository.deleteById(id);
    }

    @Override
    public boolean workoutExists(int id) {
        return workoutRepository.existsById(id);
    }
}
