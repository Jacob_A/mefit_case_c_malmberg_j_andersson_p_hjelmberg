package com.example.mefit_case.service.workout;

import com.example.mefit_case.model.Workout;
import com.example.mefit_case.service.CrudService;

public interface WorkoutService extends CrudService<Workout,Integer> {
    boolean workoutExists(int id);
}
