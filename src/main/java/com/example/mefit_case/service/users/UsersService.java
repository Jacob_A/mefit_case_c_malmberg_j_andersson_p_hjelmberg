package com.example.mefit_case.service.users;

import com.example.mefit_case.model.Users;
import com.example.mefit_case.service.CrudService;

public interface UsersService extends CrudService<Users,String> {
    boolean userExists(String id);
    Users add(String id, String firstName, String lastName);
}
