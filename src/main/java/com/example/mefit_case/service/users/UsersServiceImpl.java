package com.example.mefit_case.service.users;

import com.example.mefit_case.exceptions.MalformedRequestException;
import com.example.mefit_case.exceptions.UsersNotFoundException;
import com.example.mefit_case.model.Users;
import com.example.mefit_case.repository.UsersRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    @Override
    public Users findById(String id) {
        return this.usersRepository.findById(id).orElseThrow(() -> new UsersNotFoundException(id));
    }

    @Override
    public Collection<Users> findAll() {
        return usersRepository.findAll();
    }

    @Override
    public Users add(Users entity) {
        try {
            return usersRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public Users add(String id, String firstName, String lastName){
        if(usersRepository.existsById(id)){
            throw new Error();
        }

        Users user = new Users();
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public Users update(Users entity) {
        try {
            return usersRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public void deleteById(String id) {
        usersRepository.deleteById(id);
    }

    @Override
    public boolean userExists(String id) {
        return usersRepository.existsById(id);
    }
}
