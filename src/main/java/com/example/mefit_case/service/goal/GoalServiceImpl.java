package com.example.mefit_case.service.goal;

import com.example.mefit_case.exceptions.GoalNotFoundException;
import com.example.mefit_case.exceptions.MalformedRequestException;
import com.example.mefit_case.model.Goal;
import com.example.mefit_case.repository.GoalRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GoalServiceImpl implements GoalService {
    private final GoalRepository goalRepository;

    public GoalServiceImpl(GoalRepository goalRepository) {
        this.goalRepository = goalRepository;
    }


    @Override
    public Goal findById(Integer id) {
        return this.goalRepository.findById(id).orElseThrow(() -> new GoalNotFoundException(id));
    }

    @Override
    public Collection<Goal> findAll() {
        return goalRepository.findAll();
    }

    @Override
    public Goal add(Goal entity) {
        try{
            return goalRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public Goal update(Goal entity) {
        try{
            return goalRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public void deleteById(Integer id) {
        goalRepository.deleteById(id);
    }

    @Override
    public boolean goalExists(int id) {
        return goalRepository.existsById(id);
    }

}
