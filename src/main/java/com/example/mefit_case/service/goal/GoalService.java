package com.example.mefit_case.service.goal;

import com.example.mefit_case.model.Goal;
import com.example.mefit_case.service.CrudService;

public interface GoalService extends CrudService<Goal,Integer> {
    boolean goalExists(int id);
}