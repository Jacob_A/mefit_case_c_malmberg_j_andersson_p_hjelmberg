package com.example.mefit_case.service.exercise;

import com.example.mefit_case.exceptions.ExerciseNotFoundException;
import com.example.mefit_case.exceptions.MalformedRequestException;
import com.example.mefit_case.model.Exercise;
import com.example.mefit_case.repository.ExerciseRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ExerciseServiceImpl implements ExerciseService {
    private final ExerciseRepository exerciseRepository;

    public ExerciseServiceImpl(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }


    @Override
    public Exercise findById(Integer id) {
        return this.exerciseRepository.findById(id).orElseThrow(() -> new ExerciseNotFoundException(id));
    }

    @Override
    public Collection<Exercise> findAll() {
        return exerciseRepository.findAll();
    }

    @Override
    public Exercise add(Exercise entity) {
        try {
            return exerciseRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public Exercise update(Exercise entity) {
        try {
            return exerciseRepository.save(entity);
        } catch (DataIntegrityViolationException e){
            throw new MalformedRequestException(e.getCause().getMessage());
        }
    }

    @Override
    public void deleteById(Integer id) {
        exerciseRepository.deleteFromWorkout_exercise(id);
        exerciseRepository.deleteById(id);
    }

    @Override
    public boolean exerciseExists(int id) {
        return exerciseRepository.existsById(id);
    }
}
