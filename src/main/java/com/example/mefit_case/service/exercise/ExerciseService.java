package com.example.mefit_case.service.exercise;

import com.example.mefit_case.model.Exercise;
import com.example.mefit_case.service.CrudService;

public interface ExerciseService extends CrudService<Exercise,Integer> {
    boolean exerciseExists(int id);
}


