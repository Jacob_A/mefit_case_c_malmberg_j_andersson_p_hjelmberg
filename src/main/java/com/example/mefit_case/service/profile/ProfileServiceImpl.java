package com.example.mefit_case.service.profile;

import com.example.mefit_case.exceptions.ProfileNotFoundException;
import com.example.mefit_case.model.Profile;
import com.example.mefit_case.repository.ProfileRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ProfileServiceImpl implements ProfileService {
    private final ProfileRepository profileRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }


    @Override
    public Profile findById(Integer id) {
        if(id < 1){
            return null;
        }
        return this.profileRepository.findById(id).orElseThrow(() -> new ProfileNotFoundException(id));
    }

    @Override
    public Collection<Profile> findAll() {
        return profileRepository.findAll();
    }

    @Override
    public Profile add(Profile entity) {
        return profileRepository.save(entity);
    }

    @Override
    public Profile update(Profile entity) {
        return profileRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Profile profile = profileRepository.findById(id).orElseThrow(() -> new ProfileNotFoundException(id));
        //Did not throw error but needed for delete
        profile.getUser().setProfile(null);
        profileRepository.deleteById(id);
    }

    @Override
    public boolean profileExists(int id) {
        return profileRepository.existsById(id);
    }
}
