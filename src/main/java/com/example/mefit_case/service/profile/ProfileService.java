package com.example.mefit_case.service.profile;

import com.example.mefit_case.model.Profile;
import com.example.mefit_case.service.CrudService;

public interface ProfileService extends CrudService<Profile,Integer> {
    boolean profileExists(int id);
}