package com.example.mefit_case.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MalformedRequestException extends RuntimeException{
    public MalformedRequestException(String message){
        super("Body of request does not meet constraints. Constraint not met: " + message);
    }
}
