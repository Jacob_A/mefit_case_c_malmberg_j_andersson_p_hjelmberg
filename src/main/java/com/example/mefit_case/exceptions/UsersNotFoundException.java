package com.example.mefit_case.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UsersNotFoundException extends RuntimeException{
    public UsersNotFoundException(String id){
        super("User does not exist with ID: " + id);
    }
}
