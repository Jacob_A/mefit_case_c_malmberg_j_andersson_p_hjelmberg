package com.example.mefit_case.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SetsNotFoundException extends RuntimeException{
    public SetsNotFoundException(int id){
        super("Set does not exist with ID: " + id);
    }
}
