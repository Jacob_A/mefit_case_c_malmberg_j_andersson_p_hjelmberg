package com.example.mefit_case.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProfileNotFoundException extends RuntimeException{
    public ProfileNotFoundException(int id){
        super("Profile does not exist with ID: " + id);
    }
}
