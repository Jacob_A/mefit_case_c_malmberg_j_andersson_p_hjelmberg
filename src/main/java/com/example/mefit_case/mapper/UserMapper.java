package com.example.mefit_case.mapper;

import com.example.mefit_case.model.Profile;
import com.example.mefit_case.model.Users;
import com.example.mefit_case.model.dto.users.UsersDTO;
import com.example.mefit_case.service.profile.ProfileService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
    @Autowired
    protected ProfileService profileService;

    @Mapping(target = "profile", source= "profile.id")
    public abstract UsersDTO userToUserDTO(Users user);
    public abstract Collection<UsersDTO> userToUserDTO(Collection<Users> user);

    @Mapping(target = "profile", source= "profile", qualifiedByName = "profileIdToProfile")
    public abstract Users userDTOToUser(UsersDTO dto);

    @Named("profileIdToProfile")
    Profile mapIdToProfile(int id) {
        return this.profileService.findById(id);
    }
}
