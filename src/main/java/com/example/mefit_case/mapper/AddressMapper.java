package com.example.mefit_case.mapper;

import com.example.mefit_case.model.Address;
import com.example.mefit_case.model.Profile;
import com.example.mefit_case.model.dto.address.AddressDTO;
import com.example.mefit_case.service.profile.ProfileService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class AddressMapper {

    @Autowired
    protected ProfileService profileService;

    @Mapping(target = "profiles", source = "profiles", qualifiedByName = "profilesToIds")
    public abstract AddressDTO addressToAddressDto(Address address);
    public abstract Collection<AddressDTO> addressToAddressDto(Collection<Address> addresses);

    @Mapping(target = "profiles", source = "profiles", qualifiedByName = "profileIdsToProfiles")
    public abstract Address addressDtoToAddress(AddressDTO addressDTO);

    @Named("profileIdsToProfiles")
    Set<Profile> mapProfileIdToProfile(Set<Integer> ids){
        if(ids == null){
            return null;
        }
        return ids.stream().map(id -> profileService.findById(id)).collect(Collectors.toSet());
    }

    @Named("profilesToIds")
    Set<Integer> mapProfileToIds(Set<Profile> profiles){
        if(profiles == null){
            return null;
        }
        return profiles.stream().map(profile -> profile.getId()).collect(Collectors.toSet());
    }
}
