package com.example.mefit_case.mapper;

import com.example.mefit_case.model.*;
import com.example.mefit_case.model.dto.workout.WorkoutDTO;
import com.example.mefit_case.service.exercise.ExerciseService;
import com.example.mefit_case.service.goal.GoalService;
import com.example.mefit_case.service.program.ProgramService;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class WorkoutMapper {
    @Autowired
    protected ProgramService programService;
    @Autowired
    protected ExerciseService exerciseService;
    @Autowired
    protected GoalService goalService;

    @Mapping(target = "programs", source = "programs", qualifiedByName = "programToId")
    @Mapping(target = "exercises", source = "exercises", qualifiedByName = "exercisesToExerciseIds")
    @Mapping(target = "goals", source = "goals", qualifiedByName = "goalToId")
    public abstract WorkoutDTO workoutToWorkoutDTO(Workout workout);
    public abstract Collection<WorkoutDTO> workoutToWorkoutDTO(Collection<Workout> workout);

    @Mapping(target = "programs", source = "programs", qualifiedByName = "programIdToProgram")
    @Mapping(target = "exercises", source = "exercises", qualifiedByName = "exerciseIdsToExercises")
    @Mapping(target = "goals", source = "goals", qualifiedByName = "goalIdToGoal")
    public abstract Workout workoutDTOToWorkout(WorkoutDTO dto);

    @Named("programIdToProgram")
    public Program mapIdToProgram(int id) {
        return programService.findById(id);
    }

    @Named("exerciseIdsToExercises")
    public Exercise mapIdToExercise(int source) {
        return exerciseService.findById(source);
    }

    @Named("goalIdToGoal")
    public Goal mapIdToGoal(int id) {
        return goalService.findById(id);
    }

    @Named("programToId")
    public Set<Integer> mapProgramToId(Set<Program> source) {
        if(source == null) {
            return null;
        }
        return source.stream().map(Program::getId).collect(Collectors.toSet());
    }

    @Named("exercisesToExerciseIds")
    public Set<Integer> mapExercisesToExerciseIds(Set<Exercise> source) {
        if(source == null) {
            return null;
        }
        return source.stream().map(exercise -> exercise.getId()).collect(Collectors.toSet());
    }

    @Named("goalToId")
    public Set<Integer> mapGoalToId(Set<Goal> source) {
        if(source == null) {
            return null;
        }
        return source.stream().map(Goal::getId).collect(Collectors.toSet());
    }

}
