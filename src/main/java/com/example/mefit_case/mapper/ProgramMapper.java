package com.example.mefit_case.mapper;

import com.example.mefit_case.model.Goal;
import com.example.mefit_case.model.Profile;
import com.example.mefit_case.model.Program;
import com.example.mefit_case.model.Workout;
import com.example.mefit_case.model.dto.program.ProgramDTO;
import com.example.mefit_case.service.goal.GoalService;
import com.example.mefit_case.service.profile.ProfileService;
import com.example.mefit_case.service.workout.WorkoutService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProgramMapper {
    @Autowired
    protected ProfileService profileService;
    @Autowired
    protected WorkoutService workoutService;

    @Autowired
    protected GoalService goalService;

    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutsToProfilesIds")
    @Mapping(target = "goals", source = "goals", qualifiedByName = "goalsToGoalIds")
    public abstract ProgramDTO programToProgramDto(Program program);
    public abstract Collection<ProgramDTO> programToProgramDto(Collection<Program> program);

    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutIdsToWorkouts")
    @Mapping(target = "goals", source = "goals", qualifiedByName = "goalIdsToGoals")
    public abstract Program programDtoToProgram(ProgramDTO programDTO);


    @Named("workoutIdsToWorkouts")
    public Set<Workout> mapWorkoutIdsToWorkouts(Set<Integer> ids){
        if(ids == null){
            return null;
        }
        return ids.stream().map(id -> workoutService.findById(id)).collect(Collectors.toSet());
    }

    @Named("goalIdsToGoals")
    public Set<Goal> mapGoalIdsToGoals(Set<Integer> ids){
        if(ids == null){
            return null;
        }
        return ids.stream().map(id -> goalService.findById(id)).collect(Collectors.toSet());
    }

    @Named("workoutsToProfilesIds")
    public Set<Integer> mapWorkoutsToWorkoutsId(Set<Workout> workouts){
        if(workouts == null){
            return null;
        }
        return workouts.stream().map(workout -> workout.getId()).collect(Collectors.toSet());
    }

    @Named("goalsToGoalIds")
    public Set<Integer> mapGoalsToGoalIds(Set<Goal> goals){
        if(goals == null){
            return null;
        }
        return goals.stream().map(goal -> goal.getId()).collect(Collectors.toSet());
    }

}
