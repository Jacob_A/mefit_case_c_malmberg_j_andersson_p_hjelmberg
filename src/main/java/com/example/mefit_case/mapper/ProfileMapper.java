package com.example.mefit_case.mapper;

import com.example.mefit_case.model.*;
import com.example.mefit_case.model.dto.profile.ProfileDTO;
import com.example.mefit_case.service.address.AddressService;
import com.example.mefit_case.service.goal.GoalService;
import com.example.mefit_case.service.program.ProgramService;
import com.example.mefit_case.service.users.UsersService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProfileMapper {

    @Autowired
    protected UsersService usersService;
    @Autowired
    protected AddressService addressService;
    @Autowired
    protected ProgramService programService;
    @Autowired
    protected GoalService goalService;


    @Mapping(source = "goals", target = "goals", qualifiedByName = "goalsToGoalsIds")
    @Mapping(target = "user", source = "user.id")
    @Mapping(target = "address", source = "address.id")
    public abstract ProfileDTO profileToProfileDto(Profile profile);
    public abstract Collection<ProfileDTO> profileToProfileDto(Collection<Profile> profiles);

    @Mapping(target = "user", source = "user", qualifiedByName = "userIdToUser")
    @Mapping(target = "address", source = "address", qualifiedByName = "addressIdToAddress")
    @Mapping(target = "goals", source = "goals", qualifiedByName = "goalsIdsToGoals")
    public abstract Profile profileDtoToProfile(ProfileDTO profileDTO);

    @Named("userIdToUser")
    public Users mapUserIdToUser(String id){
        return usersService.findById(id);
    }
    @Named("addressIdToAddress")
    public Address mapAddressIdToAddress(int id){
        return addressService.findById(id);
    }
    @Named("goalsIdsToGoals")
    public Set<Goal> mapGoalIdsToGoals(Set<Integer> ids){
        if(ids == null){
            return null;
        }
        return ids.stream().map(id -> goalService.findById(id)).collect(Collectors.toSet());
    }

    @Named("goalsToGoalsIds")
    public Set<Integer> mapGoalsToGoalIds(Set<Goal> goals){
        if(goals == null){
            return null;
        }
        return goals.stream().map(goal -> goal.getId()).collect(Collectors.toSet());
    }

}
