package com.example.mefit_case.mapper;

import com.example.mefit_case.model.Exercise;
import com.example.mefit_case.model.Workout;
import com.example.mefit_case.model.dto.exercise.ExerciseDTO;
import com.example.mefit_case.service.workout.WorkoutService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ExerciseMapper {

    @Autowired
    protected WorkoutService workoutService;

    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutsToWorkoutIds")
    public abstract ExerciseDTO exerciseToExerciseDto(Exercise exercise);
    public abstract Collection<ExerciseDTO> exerciseToExerciseDto(Collection<Exercise> exercise);

    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutIdsToWorkouts")
    public abstract Exercise exerciseDtoToExercise(ExerciseDTO exerciseDTO);

    @Named("workoutIdsToWorkouts")
    public Set<Workout> mapWorkoutIdsToWorkout(Set<Integer> workoutIds){
        if(workoutIds == null){
            return null;
        }

        return workoutIds.stream().map(id -> workoutService.findById(id)).collect(Collectors.toSet());
    }

    @Named("workoutsToWorkoutIds")
    public Set<Integer> mapWorkoutIdsToWorkouts(Set<Workout> workouts){
        if(workouts == null){
            return null;
        }
        return workouts.stream().map(workout -> workout.getId()).collect(Collectors.toSet());
    }
}
