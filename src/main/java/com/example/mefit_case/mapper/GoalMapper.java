package com.example.mefit_case.mapper;

import com.example.mefit_case.model.Goal;
import com.example.mefit_case.model.Profile;
import com.example.mefit_case.model.Program;
import com.example.mefit_case.model.Workout;
import com.example.mefit_case.model.dto.goal.GoalDTO;
import com.example.mefit_case.service.profile.ProfileService;
import com.example.mefit_case.service.program.ProgramService;
import com.example.mefit_case.service.workout.WorkoutService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class GoalMapper {

    @Autowired
    protected WorkoutService workoutService;
    @Autowired
    protected ProfileService profileService;

    @Autowired
    protected ProgramService programService;

    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutsToWorkoutIds")
    @Mapping(target = "profile", source = "profile.id")
    @Mapping(target = "program", source = "program.id")
    public abstract GoalDTO goalToGoalDto(Goal goal);
    public abstract Collection<GoalDTO> goalToGoalDto(Collection<Goal> goal);

    @Mapping(target = "workouts", source = "workouts", qualifiedByName = "workoutIdsToWorkouts")
    @Mapping(target = "profile", source = "profile", qualifiedByName = "profileIdToProfile")
    @Mapping(target = "program", source = "program", qualifiedByName = "programIdToProgram")
    public abstract Goal goalDtoToGoal(GoalDTO goalDTO);

    @Named("workoutIdsToWorkouts")
    public Set<Workout> mapWorkoutToWorkoutId(Set<Integer> workoutIds){
        if(workoutIds == null){
            return null;
        }
        return workoutIds.stream().map(id -> workoutService.findById(id)).collect(Collectors.toSet());
    }

    @Named("profileIdToProfile")
    public Profile mapProfileIdToProfile(Integer id){
        return profileService.findById(id);
    }

    @Named("programIdToProgram")
    public Program mapProgramIdToProgram(Integer id){
        return programService.findById(id);
    }

    @Named("workoutsToWorkoutIds")
    public Set<Integer> mapWorkoutsToIds(Set<Workout> workouts){
        if(workouts == null){
            return null;
        }
        return workouts.stream().map(workout -> workout.getId()).collect(Collectors.toSet());
    }
}
