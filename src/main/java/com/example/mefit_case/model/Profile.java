package com.example.mefit_case.model;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int weight;
    @Column(nullable = false)
    private int height;
    private String medicalConditions;
    private String disabilities;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private Users user;
    @ManyToOne
    private Address address;
    @OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)
    private Set<Goal> goals;
}
