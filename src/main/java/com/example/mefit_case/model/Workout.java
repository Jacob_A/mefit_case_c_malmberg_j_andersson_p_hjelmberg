package com.example.mefit_case.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Workout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 30, nullable = false)
    private String name;

    @Column(length = 30, nullable = false)
    private String type;

    @Column(nullable = false)
    private boolean complete;


    @ManyToMany
    @JoinTable(
            name = "workout_exercise",
            joinColumns = {@JoinColumn(name = "workout_id")},
            inverseJoinColumns = {@JoinColumn(name = "exercise_id")}
    )
    private Set<Exercise> exercises;

    @ManyToMany(mappedBy = "workouts")
    private Set<Goal> goals;

    @ManyToMany(mappedBy = "workouts")
    private Set<Program> programs;

}
