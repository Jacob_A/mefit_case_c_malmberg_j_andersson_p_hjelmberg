package com.example.mefit_case.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 30, nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(length = 30, nullable = false)
    private String targetMuscleGroup;

    @Column(nullable = false)
    private int repetitions;

    private String imageLink;

    private String videoLink;

    @ManyToMany(mappedBy = "exercises")
    private Set<Workout> workouts;
}
