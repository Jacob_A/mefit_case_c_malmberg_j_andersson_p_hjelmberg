package com.example.mefit_case.model.dto.exercise;

import lombok.Data;

import java.util.Set;

@Data
public class ExerciseDTO {
    private int id;
    private String name;
    private String description;
    private String targetMuscleGroup;
    private int repetitions;
    private String imageLink;
    private String videoLink;
    // Sets of ids of the specified relation (Many-to-many or One to-many)
    private Set<Integer> workouts;
}
