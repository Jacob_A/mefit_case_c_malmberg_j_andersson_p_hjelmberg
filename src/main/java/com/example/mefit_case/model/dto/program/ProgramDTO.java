package com.example.mefit_case.model.dto.program;

import lombok.Data;
import java.util.Set;

@Data
public class ProgramDTO {
    private int id;
    private String name;
    private String category;
    // Sets of ids of the specified relation (Many-to-many or One to-many)
    private Set<Integer> workouts;
    private Set<Integer> goals;
}
