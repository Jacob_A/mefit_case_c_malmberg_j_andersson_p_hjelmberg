package com.example.mefit_case.model.dto.workout;

import lombok.Data;
import java.util.Set;

@Data
public class WorkoutDTO {
    private int id;
    private String name;
    private String type;
    private boolean complete;
    // Sets of ids of the specified relation (Many-to-many or One to-many)
    private Set<Integer> exercises;
    private Set<Integer> goals;
    private Set<Integer> programs;

}
