package com.example.mefit_case.model.dto.users;

import lombok.Data;

@Data
public class UsersDTO {
    private String id;
    private String firstName;
    private String lastName;
    private boolean contributor;
    private boolean admin;
    // Int for the ids of the specified relation (One-to-one or Many-to-one)
    private int profile;
}
