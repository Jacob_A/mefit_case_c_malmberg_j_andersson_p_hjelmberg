package com.example.mefit_case.model.dto.goal;

import lombok.Data;
import java.util.Set;
import java.time.LocalDate;

@Data
public class GoalDTO {
    private int id;
    private LocalDate endDate;
    private boolean achieved;
    private String name;
    // Int for the ids of the specified relation (One-to-one or Many-to-one)
    private int profile;
    private int program;
    // Sets of ids of the specified relation (Many-to-many or One to-many)
    private Set<Integer> workouts;
}
