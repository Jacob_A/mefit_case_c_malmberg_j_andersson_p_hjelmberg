package com.example.mefit_case.model.dto.address;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class AddressDTO {
    private int id;
    @NotBlank
    @NotNull
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String postalCode;
    private String city;
    private String country;
    // Sets of ids of the specified relation (Many-to-many or One to-many)
    private Set<Integer> profiles;
}
