package com.example.mefit_case.model.dto.profile;

import lombok.Data;
import java.util.Set;

@Data
public class ProfileDTO {
    private int id;
    private int weight;
    private int height;
    private String medicalConditions;
    private String disabilities;
    // Int for the ids of the specified relation (One-to-one or Many-to-one)
    private String user;
    private int address;
    // Sets of ids of the specified relation (Many-to-many or One to-many)
    private Set<Integer> goals;
}
