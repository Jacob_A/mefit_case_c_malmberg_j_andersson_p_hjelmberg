package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.AddressMapper;
import com.example.mefit_case.model.Address;
import com.example.mefit_case.model.dto.address.AddressDTO;
import com.example.mefit_case.service.address.AddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@CrossOrigin(exposedHeaders = "Location")
@RestController
@RequestMapping(path = "api/v1/address")
public class AddressController {
    private final AddressService addressService;
    private final AddressMapper addressMapper;

    public AddressController(AddressService addressService, AddressMapper addressMapper){
        this.addressService = addressService;
        this.addressMapper = addressMapper;
    }

    @Operation(summary = "Get all addresses")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AddressDTO.class)))}
                    )

    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(addressMapper.addressToAddressDto(addressService.findAll()));
    }

    @Operation(summary = "Get an address by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AddressDTO.class))}),
            @ApiResponse(responseCode = "404")
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(addressMapper.addressToAddressDto(addressService.findById(id)));
    }

    @Operation(summary = "Adds a new address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Address successfully added",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody AddressDTO addressDto){
        Address newAddress = addressMapper.addressDtoToAddress(addressDto);
        newAddress = addressService.add(newAddress);
        URI uri = URI.create("address/" + newAddress.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates an address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Address successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody AddressDTO addressDto, @PathVariable int id){
        if(addressDto.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        if(!addressService.addressExists(id)){
            return ResponseEntity.notFound().build();
        }
        AddressDTO dto = addressMapper.addressToAddressDto(addressService.update(addressMapper.addressDtoToAddress(addressDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Deletes an address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Address successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    content = @Content)
    })

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id){
        if(!addressService.addressExists(id)){
            return ResponseEntity.notFound().build();
        }
        addressService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
