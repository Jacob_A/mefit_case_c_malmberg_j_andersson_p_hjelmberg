package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.ProfileMapper;
import com.example.mefit_case.model.Profile;
import com.example.mefit_case.model.dto.profile.ProfileDTO;
import com.example.mefit_case.service.profile.ProfileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
@CrossOrigin(exposedHeaders = "Location")
@RestController
@RequestMapping("api/v1/profile")
public class ProfileController {

    private final ProfileService profileService;
    private final ProfileMapper profileMapper;

    public ProfileController(ProfileService profileService, ProfileMapper profileMapper){
        this.profileService = profileService;
        this.profileMapper = profileMapper;
    }

    @Operation(summary = "Get all profiles")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ProfileDTO.class)))}
            )
    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(profileMapper.profileToProfileDto(profileService.findAll()));
    }

    @Operation(summary = "Get profile by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ProfileDTO.class)
                            )
                    }),
            @ApiResponse(responseCode = "404", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(profileMapper.profileToProfileDto(profileService.findById(id)));
    }

    @Operation(summary = "Add a profile")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Successfully added profile",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody ProfileDTO profileDto){
        Profile newProfile = profileMapper.profileDtoToProfile(profileDto);
        newProfile = profileService.add(newProfile);
        URI uri = URI.create("profile/" + newProfile.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a profile")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully updated profile",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Profile not found",
                    content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "Unauthorized, you don't have permission to make this request",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody ProfileDTO profileDto, @PathVariable int id, @AuthenticationPrincipal Jwt principal){
        if(profileDto.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        Profile profile = profileService.findById(id);
        if(!profile.getUser().getId().equals(principal.getClaimAsString("sub"))){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User ID does not match User ID from profile");
        }
        ProfileDTO dto = profileMapper.profileToProfileDto(profileService.update(profileMapper.profileDtoToProfile(profileDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Delete a profile")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully deleted profile",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Profile could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "Unauthorized, you don't have permission to make this request",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id, @AuthenticationPrincipal Jwt principal){
        Profile profile = profileService.findById(id);
        if(!profile.getUser().getId().equals(principal.getClaimAsString("sub"))){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User ID does not match User ID from profile");
        }
        profileService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
