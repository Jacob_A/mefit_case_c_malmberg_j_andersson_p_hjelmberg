package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.ExerciseMapper;
import com.example.mefit_case.model.Exercise;
import com.example.mefit_case.model.dto.exercise.ExerciseDTO;
import com.example.mefit_case.service.exercise.ExerciseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@CrossOrigin
@RestController
@RequestMapping("api/v1/exercise")
public class ExerciseController {

    private final ExerciseService exerciseService;
    private final ExerciseMapper exerciseMapper;

    public ExerciseController(ExerciseService exerciseService, ExerciseMapper exerciseMapper){
        this.exerciseService = exerciseService;
        this.exerciseMapper = exerciseMapper;
    }
    @Operation(summary = "Get all exercises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ExerciseDTO.class)))}
            )
    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(exerciseMapper.exerciseToExerciseDto(exerciseService.findAll()));
    }

    @Operation(summary = "Get exercise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ExerciseDTO.class)
                    )
                    }),
            @ApiResponse(responseCode = "404", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(exerciseMapper.exerciseToExerciseDto(exerciseService.findById(id)));
    }

    @Operation(summary = "Add an exercise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Successfully added exercise",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity add(@RequestBody ExerciseDTO exerciseDto){
        Exercise newExercise = exerciseMapper.exerciseDtoToExercise(exerciseDto);
        newExercise = exerciseService.add(newExercise);
        URI uri = URI.create("exercise/" + newExercise.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update an exercise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully updated exercise",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Exercise not found",
                    content = @Content)
    })
    @PutMapping("{id}")
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity update(@RequestBody ExerciseDTO exerciseDto, @PathVariable int id){
        if(exerciseDto.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        if(!exerciseService.exerciseExists(id)){
            return ResponseEntity.notFound().build();
        }
        ExerciseDTO dto = exerciseMapper.exerciseToExerciseDto(exerciseService.update(exerciseMapper.exerciseDtoToExercise(exerciseDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Delete an exercise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully deleted exercise",
                    content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "You do not have permission for this request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Exercise could not be found",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity delete(@PathVariable int id){
        if(!exerciseService.exerciseExists(id)){
            return ResponseEntity.notFound().build();
        }
        exerciseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
