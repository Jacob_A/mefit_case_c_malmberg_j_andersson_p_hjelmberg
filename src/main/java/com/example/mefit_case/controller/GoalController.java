package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.GoalMapper;
import com.example.mefit_case.model.Goal;
import com.example.mefit_case.model.dto.goal.GoalDTO;
import com.example.mefit_case.service.goal.GoalService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
@CrossOrigin
@RestController
@RequestMapping("api/v1/goal")
public class GoalController {
    private final GoalService goalService;
    private final GoalMapper goalMapper;

    public GoalController(GoalService goalService, GoalMapper goalMapper){
        this.goalService = goalService;
        this.goalMapper = goalMapper;
    }

    @Operation(summary = "Get all goals")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = GoalDTO.class)))}
            )
    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(goalMapper.goalToGoalDto(goalService.findAll()));
    }

    @Operation(summary = "Get goal by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = GoalDTO.class)
                            )
                    }),
            @ApiResponse(responseCode = "404", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(goalMapper.goalToGoalDto(goalService.findById(id)));
    }

    @Operation(summary = "Add a goal")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Successfully added goal",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody GoalDTO goalDto){
        Goal newGoal = goalMapper.goalDtoToGoal(goalDto);
        newGoal = goalService.add(newGoal);
        URI uri = URI.create("exercise/" + newGoal.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a goal")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully updated goal",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Goal not found",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody GoalDTO goalDto, @PathVariable int id){
        if(goalDto.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        if(!goalService.goalExists(id)){
            return ResponseEntity.notFound().build();
        }
        GoalDTO dto = goalMapper.goalToGoalDto(goalService.update(goalMapper.goalDtoToGoal(goalDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Delete a goal")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully deleted goal",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Goal could not be found",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id){
        if(!goalService.goalExists(id)){
            return ResponseEntity.notFound().build();
        }
        goalService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
