package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.ProgramMapper;
import com.example.mefit_case.model.Program;
import com.example.mefit_case.model.dto.program.ProgramDTO;
import com.example.mefit_case.service.program.ProgramService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
@CrossOrigin
@RestController
@RequestMapping("api/v1/program")
public class ProgramController {
    private final ProgramService programService;
    private final ProgramMapper programMapper;

    public ProgramController(ProgramService programService, ProgramMapper programMapper){
        this.programService = programService;
        this.programMapper = programMapper;
    }
    @Operation(summary = "Get all Programs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProgramDTO.class)))})
    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(programMapper.programToProgramDto(programService.findAll()));
    }

    @Operation(summary = "Get program by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ProgramDTO.class))}),
            @ApiResponse(responseCode = "404")
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(programMapper.programToProgramDto(programService.findById(id)));
    }

    @Operation(summary = "Add program")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Program successfully added",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity add(@RequestBody ProgramDTO programDto){
        Program newProgram = programMapper.programDtoToProgram(programDto);
        newProgram = programService.add(newProgram);
        URI uri = URI.create("program/" + newProgram.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a program")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Program successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    content = @Content)
    })
    @PutMapping("{id}")
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity update(@RequestBody ProgramDTO programDto, @PathVariable int id){
        if(programDto.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        if(!programService.programExists(id)){
            return ResponseEntity.notFound().build();
        }
        ProgramDTO dto = programMapper.programToProgramDto(programService.update(programMapper.programDtoToProgram(programDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Delete a program")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Program successfully deleted",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity delete(@PathVariable int id){
        programService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
