package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.UserMapper;
import com.example.mefit_case.model.Users;
import com.example.mefit_case.model.dto.users.UsersDTO;
import com.example.mefit_case.service.users.UsersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;


import java.net.URI;

@CrossOrigin
@RestController
@RequestMapping("api/v1/users")
public class UsersController {
    private final UsersService usersService;
    private final UserMapper userMapper;

    public UsersController(UsersService usersService, UserMapper userMapper){
        this.usersService = usersService;
        this.userMapper = userMapper;
    }

    @Operation(summary = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = UsersDTO.class)))}
            )
    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(userMapper.userToUserDTO(usersService.findAll()));
    }

    @Operation(summary = "Get user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = UsersDTO.class)
                            )
                    }),
            @ApiResponse(responseCode = "404", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable String id){
        return ResponseEntity.ok(userMapper.userToUserDTO(usersService.findById(id)));
    }

    @Operation(summary = "Add a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Successfully added user",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody UsersDTO userDto){
        Users newUser = userMapper.userDTOToUser(userDto);
        newUser = usersService.add(newUser);
        URI uri = URI.create("users/" + newUser.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully updated user",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "User not found",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody UsersDTO usersDto, @PathVariable String id, @AuthenticationPrincipal Jwt principal){
        if(!usersDto.getId().equals(id)){
            return ResponseEntity.badRequest().build();
        }

        String jwtId = principal.getClaimAsString("sub");
        Users user = usersService.findById(id);

        if(!jwtId.equals(user.getId())){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You do not have permission to delete this user");
        }
        UsersDTO dto = userMapper.userToUserDTO(usersService.update(userMapper.userDTOToUser(usersDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Delete a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully deleted user",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "User could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "Do not have the authorization to perform this action",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id, @AuthenticationPrincipal Jwt principal){
        String jwtId = principal.getClaimAsString("sub");
        Users user = usersService.findById(id);

        if(!jwtId.equals(user.getId()) || principal.getClaimAsStringList("ROLES").contains("admin-roles-me-fit")){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You do not have permission to delete this user");
        }

        usersService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
