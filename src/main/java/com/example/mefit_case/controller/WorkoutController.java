package com.example.mefit_case.controller;

import com.example.mefit_case.mapper.WorkoutMapper;
import com.example.mefit_case.model.Workout;
import com.example.mefit_case.model.dto.workout.WorkoutDTO;
import com.example.mefit_case.service.workout.WorkoutService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@CrossOrigin
@RestController
@RequestMapping("api/v1/workout")
public class WorkoutController {

    private final WorkoutService workoutService;
    private final WorkoutMapper workoutMapper;

    public WorkoutController(WorkoutService workoutService, WorkoutMapper workoutMapper){
        this.workoutService = workoutService;
        this.workoutMapper = workoutMapper;
    }

    @Operation(summary = "Get all workouts")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = WorkoutDTO.class)))}
            )
    })
    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutDTO(workoutService.findAll()));
    }

    @Operation(summary = "Get workout by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = WorkoutDTO.class)
                            )
                    }),
            @ApiResponse(responseCode = "404", content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutDTO(workoutService.findById(id)));
    }

    @Operation(summary = "Add a workout")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Successfully added workout",
                    content = @Content)
    })
    @PostMapping
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity add(@RequestBody WorkoutDTO workoutDto){
        Workout newWorkout = workoutMapper.workoutDTOToWorkout(workoutDto);
        newWorkout = workoutService.add(newWorkout);
        URI uri = URI.create("workout/" + newWorkout.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a workout")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully updated workout",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Workout not found",
                    content = @Content)
    })
    @PutMapping("{id}")
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity update(@RequestBody WorkoutDTO workoutDto, @PathVariable int id){
        if(workoutDto.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        if(!workoutService.workoutExists(id)){
            return ResponseEntity.notFound().build();
        }
        WorkoutDTO dto = workoutMapper.workoutToWorkoutDTO(workoutService.update(workoutMapper.workoutDTOToWorkout(workoutDto)));
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Delete a workout")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully deleted workout",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Workout could not be found",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('admin-roles-me-fit')")
    public ResponseEntity delete(@PathVariable int id){
        if(!workoutService.workoutExists(id)){
            return ResponseEntity.notFound().build();
        }
        workoutService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
