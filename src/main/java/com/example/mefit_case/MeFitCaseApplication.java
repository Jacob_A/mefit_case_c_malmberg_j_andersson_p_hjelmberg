package com.example.mefit_case;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeFitCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeFitCaseApplication.class, args);
	}

}
