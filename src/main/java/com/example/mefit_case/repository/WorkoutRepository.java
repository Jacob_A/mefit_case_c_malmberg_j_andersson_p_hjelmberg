package com.example.mefit_case.repository;

import com.example.mefit_case.model.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface WorkoutRepository extends JpaRepository<Workout, Integer> {

    @Modifying
    @Query(value = "DELETE FROM goal_workout WHERE workout_id = ?1", nativeQuery = true)
    @Transactional
    void deleteFromGoal_workout(int id);
    @Modifying
    @Query(value = "DELETE FROM program_workout WHERE workout_id = ?1", nativeQuery = true)
    @Transactional
    void deleteFromProgram_workout(int id);
}
