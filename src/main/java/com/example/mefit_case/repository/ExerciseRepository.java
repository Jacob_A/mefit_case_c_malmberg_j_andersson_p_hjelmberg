package com.example.mefit_case.repository;

import com.example.mefit_case.model.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ExerciseRepository extends JpaRepository<Exercise, Integer> {
    @Modifying
    @Query(value = "DELETE FROM workout_exercise WHERE exercise_id = ?1", nativeQuery = true)
    @Transactional
    void deleteFromWorkout_exercise(int id);
}
