package com.example.mefit_case.repository;

import com.example.mefit_case.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
