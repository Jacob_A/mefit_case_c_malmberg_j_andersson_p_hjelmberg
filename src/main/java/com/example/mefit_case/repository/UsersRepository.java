package com.example.mefit_case.repository;


import com.example.mefit_case.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, String> {
}
