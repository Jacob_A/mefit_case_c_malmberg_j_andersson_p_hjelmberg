import { createHeaders } from "."

const BASE_EXERCISE_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/exercise"

export const fetchAllExercises = async () => {
    try {
        const response = await fetch(BASE_EXERCISE_URL, {
            method: "GET",
            headers: createHeaders()
        })

        if(!response.ok) throw Error("Could not fetch exercises")

        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const createExercise = async (body) => {
    try {
        const response = await fetch(BASE_EXERCISE_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not create exercise")

        return ["sucesss", null]
    } catch (error) {
        return [null, error.message]
    }
}

export const updateExercise = async (body, id) => {
    try {
        const response = await fetch(BASE_EXERCISE_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not update exercise with id: " + id)
        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const deleteExercise = async (id) => {
    try {
        const response = await fetch(BASE_EXERCISE_URL + "/" + id, {
            method: "DELETE",
            headers: createHeaders()
        })

        if(!response.ok) throw Error("Could not delete exercise with id: " + id)
        return ["success", null]
    } catch (error) {
        return [null, error.message]
    }
} 