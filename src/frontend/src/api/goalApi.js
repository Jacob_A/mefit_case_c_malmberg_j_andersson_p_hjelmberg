import { createHeaders } from "."

const GOALS_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/goal"


export const fetchAllGoals = async () => {
    try {
        const response = await fetch(GOALS_URL, {
            headers: createHeaders()
        })
        if(!response.ok) throw Error("Could not fetch goals")
        const data = await response.json()
        return [data, null]
    } catch (error) {
        return [null, error.message]
    }
}

export const fetchGoal = async (id) => {
    try{
        const response = await fetch(GOALS_URL + "/" + id,{
            method: "GET",
            headers: createHeaders()
        })
        if(!response.ok) throw Error("Could not fetch goals")
        const data = await response.json()
        return [data, null]
    } catch(error){
        return [null, error.message]
    }
}

export const addGoal = async (body) => {
    try {
        
        const response = await fetch(GOALS_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not add goal")
        return ["success", null]

    } catch (error) {
        return [null, error.message]
    }
}

export const updateGoal = async (body, id) => {
    try {
        const response = await fetch(GOALS_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not update goal with id: " + id)
        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const deleteGoal = async (id) => {
    try {
        const repsonse = await fetch(GOALS_URL + "/" + id, {
            method: "DELETE",
            headers: createHeaders()
        })

        if(!repsonse.ok) throw Error("Could not delete goal")
        return ["success", null]
    } catch (error) {
        return [null, error.message]
    }

}