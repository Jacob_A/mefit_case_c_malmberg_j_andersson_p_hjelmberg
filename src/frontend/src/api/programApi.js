import { createHeaders } from "."

const PROGRAM_BASE_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/program"

export const fetchAllPrograms = async () => {
    try {
        const response = await fetch(PROGRAM_BASE_URL, {
            method: "GET",
            headers: createHeaders()
        })

        if(!response.ok) throw Error("Could not fetch programs")

        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const addProgram = async (body) => {
    try {
        const response = await fetch(PROGRAM_BASE_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not add program")

        return ["success", null]

    } catch (error) {
        return [null, error.message]
    }
}

export const updateProgram = async (body, id) => {
    console.log(id)
    try {
        const response = await fetch(PROGRAM_BASE_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not update program")
        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}


export const deleteProgram = async (id) => {
    try {
        const response = await fetch(PROGRAM_BASE_URL + "/" + id, {
            method: "DELETE",
            headers: createHeaders(),
        })

        if(!response.ok) throw Error("Could not delete program with id: " + id)

        return ["success", null]
    } catch (error) {
        return [null, error.message]
    }
}