import { createHeaders } from "."

const BASE_PROFILE_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/profile"

export const fetchProfile = async (id) => {
    try {
        const response = await fetch(BASE_PROFILE_URL + "/" + id,{
            method: "GET",
            headers: createHeaders()
        })

        if(!response.ok) throw Error("Could not fetch profile")

        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const addProfile = async (body) => {
    try {
        const response = await fetch(BASE_PROFILE_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not create profile")
        const location = response.headers.get("Location")
        return [location, null]
    } catch (error) {
        return [null, error.message]
    }
}

export const updateProfile = async (body, id) => {
    try {
        const response = await fetch(BASE_PROFILE_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not update profile with id: " + id)
        const data = await response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const deleteProfile = async (id) => {
    try {
        const response = await fetch(BASE_PROFILE_URL + "/" + id)

        if(!response.ok) throw Error("Could not delete profile")

        return ["success", null]

    } catch (error) {
        return [null, error.message]
    }
}