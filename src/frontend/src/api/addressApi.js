import { createHeaders } from "."

const BASE_ADDRESS_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/address"

export const findAddressById = async (id) => {
    try {
        const response = await fetch(BASE_ADDRESS_URL + "/" + id, {
            headers: createHeaders()
        })
        if(!response.ok) throw Error("Could not fetch address with id: " + id)
        const data = await response.json()
        return [data, null]
    } catch (error) {
        return [null, error.message]
    }
}

export const addAddress = async (body) => {
    try {
        const response = await fetch(BASE_ADDRESS_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not add address")
        const location = response.headers.get("Location")
        return[location, null]

    } catch (error) {
        return [null, error.message]
    }
} 

export const updateAddress = async (body, id) => {
    try{
        const response = await fetch(BASE_ADDRESS_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })
        if(!response.ok) throw Error("Could not update address with id: " + id)
        const data = await response.json()
        return [data, null]
    } catch(error){
        return [null, error.message]
    }
}