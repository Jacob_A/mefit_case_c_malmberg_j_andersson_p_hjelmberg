import { createHeaders } from "."

const WORKOUT_BASE_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/workout"

export const fetchAllWorkouts = async () => {
    try {
        const response = await fetch(WORKOUT_BASE_URL, {
            method: "GET",
            headers: createHeaders()
        })

        if(!response.ok) throw Error("Could not fetch workouts")

        const data = await response.json()

        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const addWorkout = async (body) => {
    try {
        const response = await fetch(WORKOUT_BASE_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })
        
        if(!response.ok) throw Error("Could not add workout")
        return ["success", null]
    } catch (error) {
        return [null, error.message]
    }
}

export const updateWorkout = async (body, id) => {
    try {
        const response = await fetch(WORKOUT_BASE_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not update workout with id: " + id)
        const data = await response.json()

        return [data, null]
        
    } catch (error) {
        return [null, error.message]
    }
}

export const deleteWorkout = async (id) => {
    try {
        const response = await fetch(WORKOUT_BASE_URL + "/" + id, {
            method: "DELETE",
            headers: createHeaders()
        })
        if(!response.ok) throw Error("Could not delete workout")

        return ["success", null]
    } catch (error) {
        return [null, error.message]
    }
}