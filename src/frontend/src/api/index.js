import keycloak from "../keycloak"
//Crates headers used for posting data to the api

export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'Authorization': `bearer ${keycloak.token}`
    }
}
