import { createHeaders } from "."

const USER_BASE_URL = "https://me-fit-case-j-p-c.herokuapp.com/api/v1/users"

export const fetchAllUsers = async () => {
    try {
        const response = await fetch(USER_BASE_URL, {
            headers: createHeaders()
        })
        if(!response.ok) throw Error("Could not fetch users")
        const data = await response.json()
        return [data, null]
    } catch (error) {
        return [null, error.message]
    }
}

export const fetchUserById = async (id) => {
    try{
        const response = await fetch(USER_BASE_URL + "/" + id, {
            headers: createHeaders()
        })
        if(!response.ok){
            throw new Error("Could not fetch user")
        } 
        const data = await response.json()
        return data

    } catch (error){
        return error.message
    }
}

export const postUser = async (body) => {
    try{  
        const response = await fetch(USER_BASE_URL, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })
        if(!response.ok) {
            throw new Error("Could not add user")
        }
        const data = await response.json()
        return data
    } catch(error){
        return error.message
    }
}

export const updateUser = async (body, id) => {
    try {
        console.log(body, id)
        const response = await fetch(USER_BASE_URL + "/" + id, {
            method: "PUT",
            headers: createHeaders(),
            body: JSON.stringify(body)
        })

        if(!response.ok) throw Error("Could not update user with id: " + id)
        const data = response.json()
        return [data, null]

    } catch (error) {
        return [null, error.message]
    }
}

export const deleteUser = async (id) => {
    try {
        const response = await fetch(USER_BASE_URL + "/" + id, {
            method: "DELETE",
            headers: createHeaders(),
        })

        if(!response.ok) throw Error("Could not delete user with id: " + id)

        return ["success", null]

    } catch (error) {
        return [null, error.message]
    }
}