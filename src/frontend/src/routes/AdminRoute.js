import { Navigate } from "react-router-dom";

import keycloak from "../keycloak"

import React from "react";

import { ROLE } from "../Constants/roles";

//Guards routes only admin should be able to access
//Using roles from keycloak
function AdminRoute({ children, redirectTo = "/" }) {



  if (!keycloak.authenticated) {

    return <Navigate replace to={redirectTo} />;

  }

  if (keycloak.hasRealmRole(ROLE.ADMIN)) {

    return <>{children}</>;

  }


  return <Navigate replace to={redirectTo} />;

}



export default AdminRoute;