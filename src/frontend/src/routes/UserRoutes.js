import { Navigate } from "react-router-dom";

import keycloak from "../keycloak"

import React from "react";

import { ROLE } from "../Constants/roles";

//Guards routes from non users
function UserRoute({ children, redirectTo = "/" }) {

  if (!keycloak.authenticated) {
    return <Navigate replace to={redirectTo} />;
  }

  if (keycloak.hasRealmRole(ROLE.USER)) {
    return <>{children}</>;
  }

  return <Navigate replace to={redirectTo} />;

}



export default UserRoute;