import { configureStore } from "@reduxjs/toolkit";
import userReducer from './Slices/userSlice'
import programsReducer from './Slices/programsSlice'
import exercisesReducer from './Slices/exercisesSlice'
import workoutsReducer from './Slices/workoutsSlice'
import goalsReducer from './Slices/goalsSlice'

export const store = configureStore({
    reducer: {
        user: userReducer,
        programs: programsReducer,
        exercises: exercisesReducer,
        workouts: workoutsReducer,
        goals: goalsReducer,
    },
})