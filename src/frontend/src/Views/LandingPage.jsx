import LoginSignup from "../Components/Login/Login";
import './style.css'
import background from '../Media/background.jpg'

// Wrapper code to display the startup page
const Startup = () => {
  return (
    <div className="landingContainer">
      <div style={{ backgroundImage: `url(${background})` }}>
        <h1 style={{paddingTop: "50%", fontSize:"13rem"}} className="landingGreeting">Me Fit</h1>
      </div>
      <div>
        <div style={{width: "60%", marginLeft: "auto", marginRight: "auto"}}>
          <p style={{paddingTop: "100%", fontSize: "1.2rem"}}><b>Welcome the the most exciting workout you will ever have. Get inspired by programs, goals and exercises.</b></p>
          <LoginSignup></LoginSignup>
        </div>
      </div>
    </div>
  );
};

export default Startup;
