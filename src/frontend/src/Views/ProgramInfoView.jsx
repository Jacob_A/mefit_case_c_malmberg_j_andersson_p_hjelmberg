
import { useSelector } from "react-redux"
import ProgramItem from "../Components/Program/ProgramItem";



const ProgramInfoView = () => {
    const { programs } = useSelector(state => state.programs)
    return (
        <>
            <div>
            <h1 style={{textAlign: "center", paddingTop: "2rem"}}>Programs</h1>
                {programs.length > 0 ? <div>{programs.map(programs => { 
                    return <div key={programs.id}> 
                    <ProgramItem program={programs} />
                    </div>})}</div>
                     : <h3>No Program</h3>}
            </div>
        </>
    )
}

export default ProgramInfoView