import { useSelector } from "react-redux"
import WorkoutItem from "../Components/workout/WorkoutItem"

const WorkoutInfoView = () => {
const { workouts } = useSelector(state => state.workouts)
    return (
        <>
            <div>
                <h1 style={{textAlign: "center", paddingTop: "2rem"}}>Workouts</h1>
                {workouts.length > 0 ? <div>{workouts.map(workout => { 
                    return <div key={workout.id}> 
                    <WorkoutItem workout={workout} />
                    </div>})}</div>
                     : <h3>No Workouts</h3>}
            </div>
        </>
    )
}

export default WorkoutInfoView