import { useSelector } from "react-redux"
import ExerciseItem from "../Components/exercise/ExerciseItem"

const ExerciseInfoView = () => {
const { exercises } = useSelector(state => state.exercises)
    return (
        <>
            <div>
            <h1 style={{textAlign: "center", paddingTop: "2rem"}}>Exercises</h1>
                {exercises.length > 0 ? <div>{exercises.map(exercise => { 
                    return <div key={exercise.id}> 
                    <ExerciseItem exercise={exercise} />
                    </div>})}</div>
                     : <h3>No exercises</h3>}
            </div>
        </>
    )
}

export default ExerciseInfoView