
//Saves a user to local storage
export const storageSave = (key, value) => {
    if(!key && typeof(key) !== 'string'){
        throw new Error ("No storage key provided")
    }

    if(!value){
        throw new Error("No value provided for: ", key)
    }

    localStorage.setItem(key, JSON.stringify(value))
}

//Retrieves a user from local storage
export const storageRead = (key) => {

    const userData = localStorage.getItem(key)
    if(userData){
        return JSON.parse(userData)
    }
    return null
}

//Deletes a user from local storage
export const storageDelete = (key) => {
    localStorage.removeItem(key)
}