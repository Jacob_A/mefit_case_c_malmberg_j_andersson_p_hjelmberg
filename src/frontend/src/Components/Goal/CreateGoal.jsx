import { useState } from 'react'
import { useEffect } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import { useForm } from 'react-hook-form' 
import "react-datepicker/dist/react-datepicker.css"
import { fetchAllWorkouts } from '../../api/workoutApi'
import { addGoal } from '../../api/goalApi'
import WorkoutItem from '../workout/WorkoutItem'
import ProgramItem from '../Program/ProgramItem'
import { fetchAllPrograms } from '../../api/programApi'
import { useDispatch, useSelector } from 'react-redux'
import { getGoals } from '../../Slices/goalsSlice'
import './style.css'


const CreateGoal = () => {

    const { register, handleSubmit, reset } = useForm()
    const [ endDate, setEndDate ] = useState(new Date())
    const [ availableWorkouts, setAvailableWorkouts ] = useState([])
    const [ selectedWorkouts, setSelectedWorkouts ] = useState([])
    const [ availablePrograms, setAvailablePrograms ] = useState([])
    const [ selectedProgram, setSelectedProgram ] = useState(null)
    const { user } = useSelector(state => state.user)
    const dispatch = useDispatch()

    useEffect(() => {

        const fetchPrograms = async () => {
            const data = await fetchAllPrograms()
            if(data[0]){
                setAvailablePrograms(data[0])
            }
            else{
                console.log(data[1])
            }
        }

        const fetchData = async () => {
           const data = await fetchAllWorkouts()
           if(data[0]){
            setAvailableWorkouts(data[0])
           }
        }
        fetchData()
        fetchPrograms()
    },[])

    const onGoalSubmit = async (formData) => {
        
        const workoutIds = selectedWorkouts.map(workout => workout.id)
        const body ={
            name: formData.title,
            endDate: endDate,
            achieved: false,
            profile: user.profile,
            workouts: workoutIds,
            program: selectedProgram ? selectedProgram.id : null
        }

        const response = await addGoal(body)
        if(response[0]){
            dispatch(getGoals(user.profile)) 
        }
        reset()
        setSelectedWorkouts([])
        setAvailableWorkouts([...availableWorkouts, ...selectedWorkouts])
    }

    const handleSelectWorkout = (workout) => {
        //Removes a workout from available and adds to selected if a user clicks add
        setSelectedWorkouts([...selectedWorkouts, workout])
        setAvailableWorkouts(availableWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
    }

    const handleDeSelectWorkout = (workout) => {
        //Removes from selected and adds to available if user removes
        setSelectedWorkouts(selectedWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
        setAvailableWorkouts([...availableWorkouts, workout])
    }

    const handleSelectProgram = (program) => {
        //Removes a program from available and adds to selected if a user clicks add
        if(selectedProgram){
            const arr = [...availablePrograms, selectedProgram]
            setSelectedProgram(program)
            setAvailablePrograms(arr.filter(p => p.id !== program.id))
        }
        else{
            setSelectedProgram(program)
            setAvailablePrograms(availablePrograms.filter(p => p.id !== program.id))
        }
    }

    const handleDeSelectProgram = (program) => {
        //Removes from selected and adds to available if user removes
        setSelectedProgram(null)
        setAvailablePrograms([...availablePrograms, program])
    }

    return (
        <>
            <h1 style={{textAlign: "center"}}>Create Goal</h1>
            <Container>
                <Row>
                    <Col></Col>
                    <Col xs={8}> 
                        <form className='d-flex flex-column justify-content-center' onSubmit={handleSubmit(onGoalSubmit)}>
                            <label><h5>Goal Title</h5></label>
                            <input className="marginInput" {...register("title")} placeholder='Enter a goal title'></input>

                            <label><h5>End Date</h5></label>
                            <DatePicker style={{width: "100%"}} className="marginInput" selected={endDate} dateFormat="yyyy-MM-dd" onChange={(date) => setEndDate(date)} />
                            <label><h5>Selected Program</h5></label>
                            <div style={{marginLeft: "-20px"}}>
                                {selectedProgram && <div><ProgramItem program={selectedProgram}/>
                                <button style={{marginLeft: "20px"}} className='btnPrimary' onClick={() => handleDeSelectProgram(selectedProgram)}>Remove</button></div>}
                            </div>
                            <label><h5>Selected Workouts</h5></label>
                            <div className='d-flex' style={{marginLeft: "-20px"}}>
                                {selectedWorkouts.length > 0 && selectedWorkouts.map(item => {
                                    return <div key={item.id + " selected"}>
                                        <WorkoutItem workout={item} />
                                        <button style={{marginLeft: "20px"}} className='btnPrimary' onClick={() => handleDeSelectWorkout(item)}>Remove</button>
                                        </div>
                                    })
                                }
                            </div>
                            <button className='btnPrimary' type='submit'>Submit</button>
                        </form>
                    </Col>
                    <Col></Col>   
                </Row>
                <Row>
                    <Col>
                    <h2 style={{textAlign: "center", paddingTop: "2rem"}}>Available Workouts</h2>
                    {availableWorkouts.length > 0 && availableWorkouts.map(item => {
                        return <div key={item.id} style={{display:'inline-block'}}>
                                <WorkoutItem workout={item}/>
                                <button className='btnPrimary' onClick={() => handleSelectWorkout(item)}>Add</button>                        
                            </div>
                        })}
                    </Col>
                    <Col>
                    <h2 style={{textAlign: "center", paddingTop: "2rem"}}>Available Programs</h2>
                    {availablePrograms.length > 0 && availablePrograms.map(program => {
                        return <div key={program.id} style={{display:'inline-block'}}>
                                <ProgramItem program={program}></ProgramItem>
                                <button className='btnPrimary' onClick={() => handleSelectProgram(program)}>Add</button>
                        </div>
                    })}
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default CreateGoal