import { useState } from 'react'
import { useEffect } from 'react'
import { Col, Container, Row, Button } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import { useForm } from 'react-hook-form' 
import "react-datepicker/dist/react-datepicker.css"
import { fetchGoal } from '../../api/goalApi'
import WorkoutItem from '../workout/WorkoutItem'
import ProgramItem from '../Program/ProgramItem'
import { useDispatch, useSelector } from 'react-redux'
import { getPrograms } from '../../Slices/programsSlice'
import { getWorkouts } from '../../Slices/workoutsSlice'
import { updateGoals } from '../../Slices/goalsSlice'
import './style.css'

const UpdateGoal = ({ goal }) => {
    const { register, handleSubmit } = useForm()
    const [ currentGoal, setCurrentGoal ] = useState(goal)
    const [ endDate, setEndDate ] = useState(new Date(goal.endDate))
    const [ availableWorkouts, setAvailableWorkouts ] = useState([])
    const [ selectedWorkouts, setSelectedWorkouts ] = useState([])
    const [ availablePrograms, setAvailablePrograms ] = useState([])
    const [ selectedProgram, setSelectedProgram ] = useState(null)
    const { user } = useSelector(state => state.user)
    const { workouts } = useSelector(state => state.workouts)
    const { programs } = useSelector(state => state.programs)
    const dispatch = useDispatch()

    useEffect(() => {
        const fetchGoalById = async () => {
            const data = await fetchGoal(goal)
            dispatch(getPrograms())
            dispatch(getWorkouts())
            if(data[0]){
                setCurrentGoal(data[0])
                setEndDate(new Date(data[0].endDate))
            }
        }
        fetchGoalById()
    },[])

    useEffect(() => {
        if(currentGoal){
            setSelectedProgram(programs.find(x => currentGoal.program === x.id))
            setSelectedWorkouts(workouts.filter(x => currentGoal.workouts.includes(x.id)))
            setAvailablePrograms(programs.filter(x => x.id !== currentGoal.program))
            setAvailableWorkouts(workouts.filter(x => !currentGoal.workouts.includes(x.id)))
        }
    }, [programs, workouts])

    const onGoalSubmit = async (formData) => {
        
        const workoutIds = selectedWorkouts.map(workout => workout.id)

        const body = {
            id: currentGoal.id,
            name: formData.name,
            endDate: endDate,
            achieved: false,
            profile: user.profile,
            workouts: workoutIds,
            program: selectedProgram.id
        }
        dispatch(updateGoals([body, currentGoal.id]))
    }

    //Selecting removes it from the available and adds to selected
    //Deselecting removes from selected add to available
    const handleSelectWorkout = (workout) => {
        setSelectedWorkouts([...selectedWorkouts, workout])
        setAvailableWorkouts(availableWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
    }

    const handleDeSelectWorkout = (workout) => {
        setSelectedWorkouts(selectedWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
        setAvailableWorkouts([...availableWorkouts, workout])
    }

    const handleSelectProgram = (program) => {
        if(selectedProgram){
            const arr = [...availablePrograms, selectedProgram]
            setSelectedProgram(program)
            setAvailablePrograms(arr.filter(p => p.id !== program.id))
        }
        else{
            setSelectedProgram(program)
            setAvailablePrograms(availablePrograms.filter(p => p.id !== program.id))
        }
    }

    const handleDeSelectProgram = (program) => {
        setSelectedProgram(null)
        setAvailablePrograms([...availablePrograms, program])
    }

    const onFieldChange = (e) => {
        //Spread current obj, add new value to object
        setCurrentGoal({...currentGoal, [e.target.name]: e.target.value})
    }


    return(
        <>
                    <>
            <h1 style={{textAlign: "center", paddingTop: "2rem" }}>Update Goal</h1>
            <Container>
                <Row>
                    <Col></Col>
                    {currentGoal && 
                    <Col xs={8}> 
                        <form className='d-flex flex-column justify-content-center' onSubmit={handleSubmit(onGoalSubmit)}>
                            <label><h5>Goal Title</h5></label>
                            <input className="marginInput" {...register("name")} placeholder='Enter a goal title' value={currentGoal.name} onChange={onFieldChange}></input>

                            <label><h5>End Date</h5></label>
                            <DatePicker className="marginInput" selected={endDate} popperPlacement="bottom" dateFormat="yyyy-MM-dd" onChange={(date) => setEndDate(date)} />
                            <label><h5>Selected Program</h5></label>
                            <div style={{marginLeft: "-20px"}}>
                                {selectedProgram && <div><ProgramItem program={selectedProgram}/>
                                <button style={{marginLeft: "20px"}} className='btnPrimary' onClick={() => handleDeSelectProgram(selectedProgram)}>Remove</button></div>}
                            </div>
                            <label><h5>Selected Workouts</h5></label>
                            <div className='d-flex' style={{marginLeft: "-20px"}}>
                                {selectedWorkouts.length > 0 && selectedWorkouts.map(item => {
                                    return <div key={item.id + " selected"}>
                                        <WorkoutItem workout={item} />
                                        <button style={{marginLeft: "20px"}} className='btnPrimary' onClick={() => handleDeSelectWorkout(item)}>Remove</button>
                                        </div>
                                    })
                                }
                            </div>
                            <button className='btnPrimary' type='submit'>Submit</button>
                        </form>
                    </Col>}
                    <Col></Col>   
                </Row>
                <Row>
                    <Col>
                    <h2 style={{textAlign: "center"}}>Available Workouts</h2>
                    {availableWorkouts.length > 0 && availableWorkouts.map(item => {
                        return <div key={item.id} style={{display:'inline-block'}}>
                                <WorkoutItem workout={item}/>
                                <button className='btnPrimary' onClick={() => handleSelectWorkout(item)}>Add</button>                        
                            </div>
                        })}
                    </Col>
                    <Col>
                    <h2 style={{textAlign: "center"}}>Available Programs</h2>
                    {availablePrograms.length > 0 && availablePrograms.map(program => {
                        return <div key={program.id} style={{display:'inline-block'}}>
                                <ProgramItem program={program}></ProgramItem>
                                <button className='btnPrimary' onClick={() => handleSelectProgram(program)}>Add</button>
                        </div>
                    })}
                    </Col>
                </Row>
            </Container>
        </>
        </>
    )
}

export default UpdateGoal