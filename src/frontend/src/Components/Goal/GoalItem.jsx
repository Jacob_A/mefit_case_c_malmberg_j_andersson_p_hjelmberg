import { useEffect } from "react"
import { Card } from "react-bootstrap"
import { useSelector } from "react-redux"


const GoalItem = ({ goal }) => {

    const { workouts } = useSelector(state => state.workouts)
    const { programs } = useSelector(state => state.programs)

    return (
        <>
            <div style={{paddingTop: "20px", width: "100%", textAlign: "left", paddingLeft: "20px"}}>
                <h5>{goal.name}</h5>
                <Card.Body>
                    <b>Achieved:</b> {String(goal.achieved)} <br/>
                    <b>End Date:</b> {goal.endDate} <br/>
                    {goal.program !== 0 && <div style={{display: "flex"}}><b style={{paddingRight: "6px"}}>Included Program: </b>
                       <div>{programs.find(program => program.id === goal.program).name}</div>
                        </div>}
                    {workouts.length > 0 ? <div style={{display: "flex"}}><b style={{paddingRight: "6px"}}>Included Workouts: </b> {workouts.filter(workout => goal.workouts.includes(workout.id)).map(filteredWorkout => {
                        return <div key={filteredWorkout.id}>{filteredWorkout.name}</div>})}
                        </div> : <h3>No workouts in this program yet</h3>}
                </Card.Body>
            </div> 
        </>
    )
}

export default GoalItem