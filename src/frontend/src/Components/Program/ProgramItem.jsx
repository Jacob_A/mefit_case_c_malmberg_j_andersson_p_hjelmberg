
import { Card } from "react-bootstrap"
import { useSelector } from "react-redux"

const ProgramItem = ({ program }) => {


    const { workouts } = useSelector(state => state.workouts)

    return(
        <>
            <div style={{paddingTop: "20px", width: "100%", textAlign: "left", paddingLeft: "20px"}}>
                <h5>{program.name}</h5>
                <Card.Body style={{color: "#fff"}}>
                    <b >Category:</b> {program.category} <br/>
                    {workouts.length > 0 ? <div><b>Included Workouts:</b> {workouts.filter(workout => program.workouts.includes(workout.id)).map(filteredWorkout => {
                        return <div key={filteredWorkout.id}>{filteredWorkout.name}</div>})}
                        </div> : <h3>No workouts in this program yet</h3>}
                </Card.Body>
            </div>
        </>
    )
}

export default ProgramItem