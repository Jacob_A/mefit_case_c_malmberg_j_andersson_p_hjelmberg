import { Row, Col } from "react-bootstrap"
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { fetchAllWorkouts } from "../../api/workoutApi"
import WorkoutItem from "../workout/WorkoutItem"
import { addProgram } from "../../api/programApi"
import './style.css'
import { useDispatch } from "react-redux"
import { getPrograms } from "../../Slices/programsSlice"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'

const CreateProgram = () => {
    const { register, handleSubmit, reset } = useForm()
    const [ availableWorkouts, setAvailableWorkouts ] = useState([])
    const [ selectedWorkouts, setSelectedWorkouts ] = useState([])
    const dispatch = useDispatch()

    useEffect(() => {


        const fetchData = async () => {
           const data = await fetchAllWorkouts()
           if(data[0]){
            setAvailableWorkouts(data[0])
           }
        }
        fetchData()
    },[])

    const handleSelectWorkout = (workout) => {
        setSelectedWorkouts([...selectedWorkouts, workout])
        setAvailableWorkouts(availableWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
    }

    const handleDeSelectWorkout = (workout) => {
        setSelectedWorkouts(selectedWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
        setAvailableWorkouts([...availableWorkouts, workout])
    }

    const onProgramSubmit = async (formData) => {

        const workoutIds = selectedWorkouts.map(workout => workout.id)

        const body = {
            name: formData.title,
            category: formData.category,
            workouts: workoutIds
        }

        const resp = await addProgram(body)
        if(resp[0]){
            setAvailableWorkouts([...availableWorkouts, ...selectedWorkouts])
            setSelectedWorkouts([])
            dispatch(getPrograms())
            reset()
        }
        else{
            console.log(resp[1])
        }

    }

    return(
        <div style={{display: "flex", flexDirection: "column", justifyContent:"center", textAlign: "center"}}>
        <h1>Create Program</h1>
        <div>
            <div className="flexCenter">
                    <Col xs={8}> 
                        <form className='d-flex flex-column justify-content-center' onSubmit={handleSubmit(onProgramSubmit)}>
                            <label><h5>Program Title</h5></label>
                            <input className="marginInput" {...register("title")} placeholder='Enter a program title'></input>
                            <label><h5>Category</h5></label>
                            <input className="marginInput" {...register("category")} placeholder="Category name"></input>
                            <label><h5>Selected Workouts</h5></label>
                            <div className='d-flex'>
                                {selectedWorkouts.length > 0 && selectedWorkouts.map(item => {
                                    return <div key={item.id + " selected"}>
                                        <WorkoutItem workout={item} />
                                        <button onClick={() => handleDeSelectWorkout(item)} className='btnPrimary'>Remove</button>
                                        </div>
                                    })
                                }
                            </div>
                            <button type='submit' className='btnPrimary'>Submit</button>
                        </form>
                    </Col> 
                </div>
                <Row style={{width:"71%", marginLeft: "auto", marginRight: "auto"}}>
                    <Col>
                    <h2>Available Workouts</h2>
                    {availableWorkouts.length > 0 && availableWorkouts.map(item => {
                        return <div key={item.id} style={{display:'flex'}}>
                                <WorkoutItem workout={item}/>
                                <FontAwesomeIcon icon={faCheck} size='2x' style={{color: "#fff", marginTop: "2rem", cursor: "pointer"}} onClick={() => handleSelectWorkout(item)} />                        
                            </div>
                        })}
                    </Col>
                </Row>

            </div>
            </div>
    )
}

export default CreateProgram