import { Container, Row, Col } from "react-bootstrap"
import { useState } from "react"
import { useForm } from "react-hook-form"
import WorkoutItem from "../workout/WorkoutItem"
import { useDispatch } from "react-redux"
import { updatePrograms } from "../../Slices/programsSlice"
import './style.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'

const UpdateProgram = ({ program, workouts }) => {
    const { register, handleSubmit } = useForm()
    const [ currentProgram, setCurrentProgram ] = useState(program)
    const [ availableWorkouts, setAvailableWorkouts ] = useState(workouts.filter(x => !program.workouts.includes(x.id)))
    const [ selectedWorkouts, setSelectedWorkouts ] = useState(workouts.filter(x => program.workouts.includes(x.id)))
    const dispatch = useDispatch()

    const handleSelectWorkout = (workout) => {
        setSelectedWorkouts([...selectedWorkouts, workout])
        setAvailableWorkouts(availableWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
    }

    const handleDeSelectWorkout = (workout) => {
        setSelectedWorkouts(selectedWorkouts.filter(oldWorkout => oldWorkout.id !== workout.id))
        setAvailableWorkouts([...availableWorkouts, workout])
    }

    
    const onFieldChange = (e) => {
        //Spread current obj, add new value to object
        setCurrentProgram({...currentProgram, [e.target.name]: e.target.value})
    }

    const onProgramSubmit = async (formData) => {

        const workoutIds = selectedWorkouts.map(workout => workout.id)

        const body = {
            id: currentProgram.id,
            name: formData.name,
            category: formData.category,
            workouts: workoutIds
        }
        dispatch(updatePrograms([body, currentProgram.id])) //To update state in Admin dashboard
    }

    return(
        <>
            <h1 style={{textAlign: "center"}}>Update Program</h1>
            <Container>
                <Row>
                    <Col></Col>
                    {currentProgram && 
                    <Col xs={8}> 
                        <form className='d-flex flex-column justify-content-center' onSubmit={handleSubmit(onProgramSubmit)}>
                            <label><h5>Program Title</h5></label>
                            <input className="marginInput" {...register("name")} placeholder='Enter a program title' value={currentProgram.name} onChange={onFieldChange}></input>
                            <label><h5>Category</h5></label>
                            <input className="marginInput" {...register("category")} placeholder="Category name" value={currentProgram.category} onChange={onFieldChange}></input>
                            <label><h5>Selected Workouts</h5></label>
                            <div className='d-flex'>
                                {selectedWorkouts.length > 0 && selectedWorkouts.map(item => {
                                    return <div key={item.id + " selected"}>
                                        <WorkoutItem workout={item} />
                                        <button className="btnPrimary" onClick={() => handleDeSelectWorkout(item)}>Remove</button>
                                        </div>
                                    })
                                }
                            </div>
                            <button className="btnPrimary" type='submit'>Submit</button>
                        </form>
                    </Col>}
                    <Col></Col>   
                </Row>
                <Row style={{width:"71%", marginLeft: "auto", marginRight: "auto", marginTop: "2rem"}}>
                    <Col>
                    <h2 style={{textAlign: "center"}}>Available Workouts</h2>
                    {availableWorkouts.length > 0 && availableWorkouts.map(item => {
                        return <div key={item.id} style={{display:'flex'}}>
                                <WorkoutItem workout={item}/>
                                <FontAwesomeIcon icon={faCheck} size='2x' style={{color: "#fff", marginTop: "2rem", cursor: "pointer"}} onClick={() => handleSelectWorkout(item)} />                        
                            </div>
                        })}
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default UpdateProgram