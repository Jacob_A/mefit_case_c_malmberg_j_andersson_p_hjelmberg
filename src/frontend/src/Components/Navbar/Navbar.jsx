import { useSelector } from "react-redux";
import './Navbar.css'


// Nav-bar, placed above the app routing to exist for all pages
const Navbar = () => {

  const { user } = useSelector(state => state.user);

  return (
    <nav className="topbar" >
        <div className="topBarContainer" style={{ display: "flex", justifyContent: "left", fontSize: "18px"}}>
        <span>Hi, <b className="highlight">{user.firstName}!</b></span>
        <span>Welcome back to your <span className="highlight"><b>Dashboard</b></span></span>  
      </div>
    </nav>
  );
};

export default Navbar;
