import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDumbbell, faArrowRightFromBracket, faArrowRight, faBars } from '@fortawesome/free-solid-svg-icons'
import './style.css'
import keycloak from '../../keycloak';
import { useNavigate } from 'react-router-dom';


function AdminSidebar() {

    const navigate = useNavigate()

    function handleLogout() {
        keycloak.logout();
    }
  return (
    <section>

    <div className="top">
          <div className="brand">
            <FontAwesomeIcon icon={faDumbbell} size='3x' style={{color: "#ffc107"}} />
            <h1>Me Fit</h1> 
            <div>
            <div className="hamburgerMenu">
            <FontAwesomeIcon icon={faBars} style={{color: "#ffc107"}}/>
            </div>  
          </div>
          </div>
          <div className="links">
            <ul>
              <li>
                <button className='menuBtn' onClick={() => navigate("/profile")}><FontAwesomeIcon className='menuIcon' icon={faArrowRight}/> Profile</button>
              </li>
              </ul>
          </div>
        </div>
        <div className="logout">
            <span className="logout" onClick={handleLogout}><FontAwesomeIcon icon={faArrowRightFromBracket} style={{paddingRight: "15px"}}/>Logout</span>
        </div>
      </section>
  )
}


export default AdminSidebar