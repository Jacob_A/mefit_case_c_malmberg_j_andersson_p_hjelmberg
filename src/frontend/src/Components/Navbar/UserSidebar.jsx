import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDumbbell, faArrowRightFromBracket, faArrowRight, faBars } from '@fortawesome/free-solid-svg-icons'
import './style.css'
import keycloak from '../../keycloak';

function AdminSidebar( { displayPrograms, displayWorkouts, displayExercises } ) {

    function handleLogout() {
        keycloak.logout();
    }

  return (
    <section>

    <div className="top">
          <div className="brand">
            <FontAwesomeIcon icon={faDumbbell} size='3x' style={{color: "#ffc107"}} />
            <h1>Me Fit</h1> 
            <div>
            <div className="hamburgerMenu">
            <FontAwesomeIcon icon={faBars} style={{color: "#ffc107"}}/>
            </div>  
          </div>
          </div>
          <div className="links">
            <ul>
              <li>
              <button className='menuBtn' onClick={() => displayPrograms(true)}><FontAwesomeIcon className='menuIcon' icon={faArrowRight}/> Programs</button>
              </li>
              <li>
              <button className='menuBtn' onClick={() => displayWorkouts(true)}><FontAwesomeIcon className='menuIcon' icon={faArrowRight}/> Workouts</button>
              </li>
              <li>
              <button className='menuBtn' onClick={() => displayExercises(true)}><FontAwesomeIcon className='menuIcon' icon={faArrowRight}/> Exercises</button>
              </li>
              </ul>
          </div>
        </div>
        <div className="logout">
            <span className="logout" onClick={handleLogout}><FontAwesomeIcon icon={faArrowRightFromBracket} style={{paddingRight: "15px"}}/>Logout</span>
        </div>
      </section>
  )
}


export default AdminSidebar