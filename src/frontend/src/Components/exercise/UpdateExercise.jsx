import { useForm } from "react-hook-form"
import { Container, Row, Col, Button } from "react-bootstrap"
import { useState } from "react"
import { useDispatch } from "react-redux"
import { updateExercises } from "../../Slices/exercisesSlice"


const UpdateExercise = ({ exercise }) => {

    const { register, handleSubmit, reset, formState: { errors } } = useForm()
    const [ currentExercise, setCurrentExercise ] = useState(exercise)
    const dispatch = useDispatch()


    const onFieldChange = (e) => {
        //Spread current obj, add new value to object
        //Field is chosen by e.target.name
        setCurrentExercise({...exercise, [e.target.name]: e.target.value})
    }

    const onExerciseSubmit = async (formData) => {
        const body = {
            id: exercise.id,
            name: formData.name,
            description: formData.description,
            repetitions: formData.repetitions,
            targetMuscleGroup: formData.targetMuscleGroup,
            imageLink: formData.imageLink.length > 0 ? formData.imageLink : null,
            videoLink: formData.videoLink.length > 0 ? formData.videoLink : null,
            workouts: exercise.workouts
        }

        dispatch(updateExercises([body, currentExercise.id])) //Updates and sets new exercise into state
        reset() //Form reset
    }


    return(
        <>
            <Container>
                <Row>
                    <Col></Col>
                    {currentExercise && 
                    <Col xs={8}> 
                        <form className='d-flex flex-column justify-content-center' onSubmit={handleSubmit(onExerciseSubmit)}>
                            <label><h3>Exercise Title</h3></label>
                            <input {...register("name", {required: true})} placeholder='Enter a exercise title' value={currentExercise.name} onChange={onFieldChange}></input>
                            {errors.title && <span className="bg-danger mt-1">Title is required</span>}
                            <label><h3>Description</h3></label>
                            <input {...register("description", {required: true})} placeholder='Enter exercise description' value={currentExercise.description} onChange={onFieldChange}></input>
                            <label><h3>Repetitions</h3></label>
                            <input {...register("repetitions", {required: true})} placeholder='Enter exercise repetitions' value={currentExercise.repetitions} onChange={onFieldChange}></input>
                            {errors.desc && <span className="bg-danger mt-1">Description is required</span>}
                            <label><h3>Target Muscle Groups</h3></label>
                            <input {...register("targetMuscleGroup", {required: true})} placeholder='Enter muscle groups' value={currentExercise.targetMuscleGroup} onChange={onFieldChange}></input>
                            {errors.target && <span className="bg-danger mt-1">Target Muscle Groups is required</span>}
                            <label><h3>Image Link (optional)</h3></label>
                            <input {...register("imageLink")} placeholder='Image URL' value={currentExercise.imageLink ? currentExercise.imageLink : ""} onChange={onFieldChange}></input>
                            <label><h3>Video Link (optional)</h3></label>
                            <input {...register("videoLink")} placeholder='Video URL' value={currentExercise.videoLink ? currentExercise.videoLink : ""} onChange={onFieldChange}></input>
                            <Button className="mt-4" type='submit'>Submit</Button>
                        </form>
                    </Col> }
                    <Col></Col>   
                </Row>
            </Container>
        </>
    )
}

export default UpdateExercise