import { Card } from "react-bootstrap"
import './style.css'

const ExerciseItem = ({ exercise }) => {

    return(
        <>
            <div style={{paddingTop: "20px", width: "100%", textAlign: "left", paddingLeft: "20px"}}>
                <h5>{exercise.name}</h5>
                {exercise.imageLink && <Card.Img variant="top" src={exercise.imageLink} /> }
                <Card.Body>
                    <p className="fontColor"><b>Description: </b>{exercise.description}</p>
                    <p className="fontColor"><b>Target Muscle Group: </b>{exercise.targetMuscleGroup}</p>
                    <p className="fontColor"><b>Repetitions: </b>{exercise.repetitions}</p>
                    {exercise.videoLink && <a href={exercise.videoLink} className="fontColor">Video Tutorial Link</a>}
                </Card.Body>
            </div> 
        </>
    )
}

export default ExerciseItem