import { useForm } from "react-hook-form"
import { Row, Col } from "react-bootstrap"
import { createExercise } from "../../api/exerciseApi"
import './style.css'
import { useDispatch } from "react-redux"
import { getExercises } from "../../Slices/exercisesSlice"

const CreateExercise = () => {

    const { register, handleSubmit, reset, formState: { errors } } = useForm()
    const dispatch = useDispatch()

    const onExerciseSubmit = async (formData) => {
        const body = {
            name: formData.title,
            description: formData.desc,
            targetMuscleGroup: formData.target,
            repetitions: formData.repetitions,
            imageLink: formData.img.length > 0 ? formData.img : null,
            videoLink: formData.vid.length > 0 ? formData.vid : null
        }
        //TODO alert
        const data = await createExercise(body)
        if(data[0]) dispatch(getExercises()) //Re fetches the exercises after add
        reset() //Resets the form
    }


    return(
        <div style={{display: "flex", flexDirection: "column", justifyContent:"center", textAlign: "center"}}>
        <h1>Create Exercise</h1>
        <div>
            <div className="flexCenter">
                
                <Row style={{width: "50%"}}>

                    <Col xs={8} style={{width: '100%'}}> 
                        <form style={{display: "flex", flexDirection: "column"}} onSubmit={handleSubmit(onExerciseSubmit)}>
                            <label><h5>Exercise Title</h5></label>
                            <input className="marginInput" {...register("title", {required: true})} placeholder='Enter a exercise title'></input>
                            {errors.title && <span className="bg-danger mt-1">Title is required</span>}
                            <label><h5>Description</h5></label>
                            <input className="marginInput"  {...register("desc", {required: true})} placeholder='Enter exercise description'></input>
                            <label><h5>Repetitions</h5></label>
                            <input className="marginInput" {...register("repetitions", {required: true})} placeholder='Enter exercise repetitions'></input>
                            {errors.desc && <span className="bg-danger mt-1">Description is required</span>}
                            <label><h5>Target Muscle Groups</h5></label>
                            <input className="marginInput" {...register("target", {required: true})} placeholder='Enter muscle groups'></input>
                            {errors.target && <span className="bg-danger mt-1">Target Muscle Groups is required</span>}
                            <label><h5>Image Link (optional)</h5></label>
                            <input className="marginInput" {...register("img")} placeholder='Image URL'></input>
                            <label><h5>Video Link (optional)</h5></label>
                            <input className="marginInput" {...register("vid")} placeholder='Video URL'></input>
                            <button className='btnPrimary' type='submit'>Submit</button>
                        </form>
                    </Col>
                </Row>
            </div>
        </div>
        </div>
    )
}

export default CreateExercise