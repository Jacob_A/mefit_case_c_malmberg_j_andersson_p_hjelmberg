import { useState } from "react"
import { useEffect } from "react"
import { Container, Row, Col, Button } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { useDispatch } from "react-redux"
import { fetchAllExercises } from "../../api/exerciseApi"
import { addWorkout } from "../../api/workoutApi"
import { getWorkouts } from "../../Slices/workoutsSlice"
import ExerciseItem from "../exercise/ExerciseItem"
import './style.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'

const CreateWorkout = () =>{

    const { register, handleSubmit, reset } = useForm()

    const [ exercises, setExercises ] = useState([])
    const [ selectedExercises, setSelectedExercises ] = useState([])
    const dispatch = useDispatch()

    const onWorkoutSubmit = async (formData) => {

        const exerciseIds = selectedExercises.map(exercises => exercises.id)

        const body = {
            name: formData.title,
            type: formData.type,
            complete: false,
            exercises: exerciseIds
        }

        const resp = await addWorkout(body)
        if(resp){
            setExercises([...exercises, ...selectedExercises])
            setSelectedExercises([])
            dispatch(getWorkouts())
            reset()
        }
    }

    useEffect(() => {
        const fetchExercises = async () => {
            const exerciseData = await fetchAllExercises()
            if(exerciseData[0]){
                setExercises(exerciseData[0])
            }
            else{
                console.log(exerciseData[1])
            }
        }
        fetchExercises()
    }, [])

    const selectExercise = (exercise) => {
        setSelectedExercises([...selectedExercises, exercise])
        setExercises(exercises.filter(prev => prev.id !== exercise.id))
    }

    const deSelectExercise = (exercise) => {
        setSelectedExercises(selectedExercises.filter(prev => prev.id !== exercise.id))
        setExercises([...exercises, exercise])
    }

    return (
        <div style={{display: "flex", flexDirection: "column", justifyContent:"center", textAlign: "center"}}>
            <h1>Create Workout</h1>
            <div style={{display: "flex", flexDirection: "column", justifyContent: "center"}}>
                <div className="flexCenter">
                    
                    <Col xs={8}> 
                        <form style={{display: "flex", flexDirection: "column"}} onSubmit={handleSubmit(onWorkoutSubmit)}>
                            <label><h5>Workout Title</h5></label>
                            <input className="marginInput" {...register("title")} placeholder='Enter a workout title'></input>
                            <label><h5>Workout Type</h5></label>
                            <input className='marginInput' {...register("type")} placeholder='Enter a workout type'></input>

                            <label><h5>Selected Workouts</h5></label>
                            <div className='d-flex'>
                                {selectedExercises.length > 0 && selectedExercises.map(item => {
                                    return <div key={item.id + " selected"}>
                                            <ExerciseItem exercise={item}/>
                                            <button onClick={() => deSelectExercise(item)} className='btnPrimary'>Remove</button>
                                        </div>
                                    })
                                }
                            </div>
                            <button type='submit' className='btnPrimary'>Submit</button>
                        </form>
                    </Col>
                      
                </div>
                <Row style={{width:"71%", marginLeft: "auto", marginRight: "auto"}}>
                    <h2>Available Exercises</h2>
                    {exercises.length > 0 && exercises.map(item => {
                        return <div key={item.id} style={{display:'flex'}}>
                                <ExerciseItem exercise={item}/>
                                <FontAwesomeIcon icon={faCheck} size='2x' style={{color: "#fff", marginTop: "2rem", cursor: "pointer"}} onClick={() => selectExercise(item)} />                    
                            </div>
                        })}
                </Row>
            </div>
        </div>
    )
}

export default CreateWorkout