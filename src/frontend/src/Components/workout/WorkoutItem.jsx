import { Card, Button } from "react-bootstrap"
import { useSelector } from "react-redux"
import '../Dashboards/adminstyle.css'


const WorkoutItem = ({ workout }) => {

    const { exercises } = useSelector(state => state.exercises)
    
    return (
        <>
            <div key={1} style={{paddingTop: "20px", width: "100%", textAlign: "left", paddingLeft: "20px"}}>
                <h5>{workout.name}</h5>
                <Card.Body>
                <p style={{color: "#fff", marginBottom: "0px"}}><b>Type:</b> {workout.type} </p>
                <p style={{color: "#fff", marginBottom: "0px"}}><b>Complete:</b> {String(workout.complete)} </p>
                {exercises.length > 0 ? <div style={{color: "#fff", marginBottom: "0px"}}><b>Included Exercises:</b> {exercises.filter(exercise => workout.exercises.includes(exercise.id))
                .map(filteredExercise => {
                        return <div key={filteredExercise.id}>{filteredExercise.name}</div>})}
                        </div> : <h3>No exercises in this workout yet</h3>}
                </Card.Body>
            </div>
        </>
    )
}

export default WorkoutItem