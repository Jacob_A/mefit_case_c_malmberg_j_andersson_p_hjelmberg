import { useState } from "react"
import { Container, Row, Col } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { useDispatch } from "react-redux"
import { updateWorkouts } from "../../Slices/workoutsSlice"
import ExerciseItem from "../exercise/ExerciseItem"
import './style.css'

const UpdateWorkout = ({ workout, exercises, syncWorkout }) =>{

    const { register, handleSubmit } = useForm()

    const [ currentWorkout, setCurrentWorkout ] = useState(workout)
    const [ availableExercises, setAvailableExercises ] = useState(exercises.filter(x => !workout.exercises.includes(x.id)))
    const [ selectedExercises, setSelectedExercises ] = useState(exercises.filter(x => workout.exercises.includes(x.id)))
    const dispatch = useDispatch()

    const onWorkoutSubmit = async (formData) => {

        const exerciseIds = selectedExercises.map(exercises => exercises.id)

        const body = {
            id: currentWorkout.id,
            name: formData.name,
            type: formData.type,
            complete: false,
            exercises: exerciseIds,
            goals: currentWorkout.goals,
            programs: currentWorkout.programs
        }
        dispatch(updateWorkouts([body, currentWorkout.id]))
    }

    const selectExercise = (exercise) => {
        setSelectedExercises([...selectedExercises, exercise])
        setAvailableExercises(availableExercises.filter(prev => prev.id !== exercise.id))
    }

    const deSelectExercise = (exercise) => {
        setSelectedExercises(selectedExercises.filter(prev => prev.id !== exercise.id))
        setAvailableExercises([...availableExercises, exercise])
    }

    const onFieldChange = (e) => {
        //Spread current obj, add new value to object
        setCurrentWorkout({...currentWorkout, [e.target.name]: e.target.value})
    }

    return (
        <div style={{display: "flex", flexDirection: "column", justifyContent:"center", textAlign: "center"}}>
            <h1>Update Workout</h1>
            <Container>
                <Row>
                    <Col></Col>
                    {currentWorkout && 
                    <Col xs={8}> 
                        <form className='d-flex flex-column justify-content-center' onSubmit={handleSubmit(onWorkoutSubmit)}>
                            <label className="fontColor">Workout Title</label>
                            <input {...register("name")} placeholder='Enter a workout title' value={currentWorkout.name} onChange={onFieldChange}></input>
                            <label className="fontColor">Workout Type</label>
                            <input {...register("type")} placeholder='Enter a workout type' value={currentWorkout.type} onChange={onFieldChange}></input>
                            <button type='submit' className="btnPrimary">Submit</button>                           
                            
                            <div style={{marginTop: "1.5rem", color: "#ffc107"}}>
                                <h2 style={{color: "#ffc107"}}>Selected Workouts</h2>
                                {selectedExercises.length > 0 && selectedExercises.map(item => {
                                    return <div key={item.id + " selected"}>
                                            <ExerciseItem exercise={item}/>
                                            <button onClick={() => deSelectExercise(item)}>Remove</button>
                                        </div>
                                    })
                                }
                            </div>
                        </form>
                    </Col>}
                    <Col></Col>   
                </Row>
                <Row>
                    <h2 style={{paddingTop: "1.5rem"}}>Available Exercises</h2>
                    {exercises.length > 0 && availableExercises.map(item => {
                        return <div key={item.id}>
                                <ExerciseItem exercise={item}/>
                                <button onClick={() => selectExercise(item)}>Add</button>                        
                            </div>
                        })}
                </Row>
            </Container>
        </div>
    )
}

export default UpdateWorkout