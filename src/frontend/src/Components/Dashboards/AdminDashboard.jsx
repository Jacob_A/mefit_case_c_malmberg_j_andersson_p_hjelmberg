import React, { useEffect, useState } from 'react'
import './adminstyle.css'
import AdminSidebar from '../Navbar/AdminSidebar';
import Navbar from '../Navbar/Navbar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDumbbell, faPenToSquare, faUser, faXmark } from '@fortawesome/free-solid-svg-icons'
import Modal from 'react-modal';
import ExerciseItem from '../exercise/ExerciseItem';
import ProgramItem from '../Program/ProgramItem';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAllUsers } from '../../api/userApi';
import WorkoutItem from '../workout/WorkoutItem';
import CreateWorkout from '../workout/CreateWorkout';
import CreateExercise from '../exercise/CreateExercise';
import CreateProgram from '../Program/CreateProgram';
import UpdateProgram from '../Program/UpdateProgram';
import UpdateExercise from '../exercise/UpdateExercise';
import UpdateWorkout from '../workout/UpdateWorkout';
import { deletePrograms, getPrograms } from '../../Slices/programsSlice';
import { deleteExercises, getExercises } from '../../Slices/exercisesSlice';
import { deleteWorkouts, getWorkouts } from '../../Slices/workoutsSlice';


function AdminDashboard() {


  const [ users, setUsers ] = useState([])
  const { programs } = useSelector(state => state.programs)
  const { exercises } = useSelector(state => state.exercises)
  const { workouts } = useSelector(state => state.workouts)
  const dispatch = useDispatch()
  const [ workoutModalIsOpen, setWorkoutModalIsOpen ] = useState(false);
  const [ exercisesModalIsOpen, setExercisesModalIsOpen ] = useState(false);
  const [ programModalIsOpen, setProgramModalIsOpen ] = useState(false);
  const [ editProgramModalIsOpen, setEditProgramModalIsOpen ] = useState(false)
  const [ editExercisesModalIsOpen, setEditExercisesModalIsOpen ] = useState(false);
  const [ editWorkoutModalIsOpen, setEditWorkoutModalIsOpen ] = useState(false)
  const [ selectedProgram, setSelectedProgram ] = useState(null)
  const [ selectedExercise, setSelectedExercise ] = useState(null)
  const [ selectedWorkout, setSelectedWorkout ] = useState(null)

  useEffect(() => {
    const fetchData = async () => {
      const userData = await fetchAllUsers()
      dispatch(getPrograms())
      dispatch(getExercises())
      dispatch(getWorkouts()) 

      if(userData[0]) setUsers(userData[0])
    }
    fetchData()
  }, [])

  const removeWorkout = async (id) => {
    dispatch(deleteWorkouts(id))
  }

  const removeExercise = async (id) => {
    dispatch(deleteExercises(id))
  }

  const removeProgram = async (id) => {
    dispatch(deletePrograms(id))
  }

  //Opens the correct modals and sets the object to be edited
  const handleProgEdit = (program) => {
    setSelectedProgram(program)
    setEditProgramModalIsOpen(true)
  }

  const handleExerciseEdit = (exercise) => {
    setSelectedExercise(exercise)
    setEditExercisesModalIsOpen(true)
  }

  const handleWorkoutEdit = (workout) => {
    setSelectedWorkout(workout)
    setEditWorkoutModalIsOpen(true)
  }

  return (
    <>
    <AdminSidebar/>
          <div className='dashboardContainer'>
            <Navbar />
                 <div className="grid">
                    <div className="rowOne">
                      <div>
                        <div className='gridHeader'>
                          <h3>Members</h3>
                          <p>Current count of active MeFit members</p>
                        </div>
                        <div className='gridContent'>
                          <p>{users.length}</p>
                          <div className='iconContainer'>
                            <FontAwesomeIcon icon={faUser} size='4x' style={{color: "#fff", marginTop: "1rem"}} />
                          </div>
                        </div>
                      </div>
                      <div>
                        <div className='gridHeader'>
                          <h3>Workout programs</h3>
                          <p>Amount of stored programs for members</p>
                        </div>
                        <div className='gridContent'>
                          <p>{programs.length}</p>
                          <div className='iconContainer'>
                            <FontAwesomeIcon icon={faDumbbell} size='4x' style={{color: "#fff", marginTop: "1rem"}} />
                          </div>
                        </div>
                      </div>

                    </div>
                    <div className="rowTwo">
                    <div>
                        <h3 className='gridHeader'>Exercises</h3>
                        <button className='btnPrimary' onClick={() => setExercisesModalIsOpen(true)}>Add Exercise</button>
                          <Modal isOpen={exercisesModalIsOpen} onRequestClose={() => setExercisesModalIsOpen(false)}>
                            <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setExercisesModalIsOpen(false)} />
                            <CreateExercise />                            
                          </Modal>
                        {exercises.length > 0 ? <div style={{paddingBottom: "1.5rem"}}>{exercises.map(exercise => {
                          return <div style={{display: "flex", width: "90%"}} key={exercise.id}>
                            <ExerciseItem exercise={exercise}/>
                            <div style={{display: "flex", justifyContent: "end", width: "5%", marginRight: "-20px"}}>
                            <FontAwesomeIcon className='deleteIcon' icon={faXmark} onClick={() => removeExercise(exercise.id)} />
                            <FontAwesomeIcon className='editIcon' icon={faPenToSquare} size='1x' onClick={() => handleExerciseEdit(exercise)} />
                            </div>
                            </div>
      
                        })} </div> : <h3>No exercises available</h3>}
                            <Modal ariaHideApp={false} isOpen={editExercisesModalIsOpen} onRequestClose={() => setEditExercisesModalIsOpen(false)}>
                            <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} />
                            <UpdateExercise exercise={selectedExercise} />                            
                          </Modal>
                      </div>
                      <div>
                        <h3 className='gridHeader'>Workouts</h3>
                        <button className='btnPrimary' onClick={() => setWorkoutModalIsOpen(true)}>Add Workout</button>
                          <Modal isOpen={workoutModalIsOpen} onRequestClose={() => setWorkoutModalIsOpen(false)}>
                            <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setWorkoutModalIsOpen(false)} />
                            <CreateWorkout />                            
                          </Modal>

                        {workouts.length > 0 ? <div style={{paddingBottom: "1.5rem"}}>
                          {workouts.map(workout => {
                            return <div style={{display: "flex", width: "90%"}} key={workout.id}>
                              <WorkoutItem workout={workout}/>
                              <div style={{display: "flex", justifyContent: "end", width: "5%", marginRight: "-20px"}}>
                              <FontAwesomeIcon className='deleteIcon' icon={faXmark} size='1x' onClick={() => removeWorkout(workout.id)} />
                              <FontAwesomeIcon className='editIcon' icon={faPenToSquare} size='1x' onClick={() => handleWorkoutEdit(workout)} />
                              </div>
                              </div>
                          })}
                        </div> : <h3>No workouts available</h3>}
                        <Modal ariaHideApp={false} isOpen={editWorkoutModalIsOpen} onRequestClose={() => setEditWorkoutModalIsOpen(false)}>
                            <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setWorkoutModalIsOpen(false)} />
                            <UpdateWorkout workout={selectedWorkout} exercises={exercises}/>                            
                          </Modal>
                      </div>
                      <div>
                        <h3 className='gridHeader'>Programs</h3>
                        <button className='btnPrimary' onClick={() => setProgramModalIsOpen(true)}>Add program</button>
                        <Modal isOpen={programModalIsOpen} onRequestClose={() => setProgramModalIsOpen(false)}>
                            <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setProgramModalIsOpen(false)} />
                            <CreateProgram />                            
                          </Modal>
                        {programs.length > 0 ? <div style={{paddingBottom: "1.5rem"}}>{programs.map(program => {
                          return <div style={{display: "flex", width: "90%"}} key={program.id}>
                            <ProgramItem program={program}/>
                            <div style={{display: "flex", justifyContent: "end", width: "5%", marginRight: "-20px"}}>
                            <FontAwesomeIcon className='deleteIcon' icon={faXmark} size='1x' onClick={() => removeProgram(program.id)} />
                            <FontAwesomeIcon className='editIcon' icon={faPenToSquare} size='1x' onClick={() => handleProgEdit(program)} />
                            </div>
                            </div>
                        })}</div> : <h3>No Programs available</h3>}
                            <Modal ariaHideApp={false} isOpen={editProgramModalIsOpen} onRequestClose={() => setEditProgramModalIsOpen(false)}>
                            <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setEditProgramModalIsOpen(false)} />
                            <UpdateProgram program={selectedProgram} workouts={workouts}/>                          
                            </Modal>
                      </div>
                    </div>
            </div> 
        </div>
    </>
  )
}

export default AdminDashboard