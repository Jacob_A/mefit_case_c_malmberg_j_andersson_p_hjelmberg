import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchProfile } from "../../api/profileApi"
import GoalItem from "../Goal/GoalItem"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faXmark, faCheck, faPenToSquare } from '@fortawesome/free-solid-svg-icons'
import './userstyle.css'
import CreateProfile from "../Profile/CreateProfile"
import Modal from 'react-modal';
import UpdateProfile from "../Profile/UpdateProfile"
import UpdateGoal from "../Goal/UpdateGoal"
import UserSidebar from "../Navbar/UserSidebar"
import Navbar from "../Navbar/Navbar"
import CreateGoal from "../Goal/CreateGoal"
import { deleteGoals, getGoals } from "../../Slices/goalsSlice"
import { getExercises } from "../../Slices/exercisesSlice"
import { getWorkouts } from "../../Slices/workoutsSlice"
import { getPrograms } from "../../Slices/programsSlice"
import ProgramInfoView from "../../Views/ProgramInfoView"
import WorkoutInfoView from "../../Views/WorkoutInfoView"
import ExerciseInfoView from "../../Views/ExerciseInfoView"


const UserDashboard = () => {

    const [ profile, setProfile ] = useState(null)
    const { goals } = useSelector(state => state.goals)
    const { user } = useSelector(state => state.user)
    const [ goalModalIsOpen, setGoalModalIsOpen ] = useState(false);
    const [ profileModalIsOpen, setProfileModalIsOpen ] = useState(false);
    const [ selectedGoal, setSelectedGoal ] = useState(null)
    const [ addGoalModalIsOpen, setAddGoalModalIsOpen ] = useState(false)
    const [ viewProgramModalIsOpen, setViewProgramModalIsOpen ] = useState(false)
    const [ viewWorkoutModalIsOpen, setViewWorkoutModalIsOpen ] = useState(false)
    const [ viewExerciseModalIsOpen, setViewExerciseModalIsOpen ] = useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        const getProfile = async () => {
            const fetched = await fetchProfile(user.profile)
            if(fetched[0]){
                setProfile(fetched[0])
                dispatch(getGoals(user.profile))
                //Fetches and sets objects in state
                dispatch(getExercises())
                dispatch(getPrograms())
                dispatch(getWorkouts())
            }
        }
        getProfile()
    
    },[user])


    const removeGoal = (id) => {
      dispatch(deleteGoals(id))
    }

    //Opens the modal and sets goal to be edited
    const handleGoalEdit = (goal) => {
      setSelectedGoal(goal)
      setGoalModalIsOpen(true)
    }



    return (
        <>
        {user.profile && <UserSidebar displayPrograms={setViewProgramModalIsOpen} displayWorkouts={setViewWorkoutModalIsOpen} displayExercises={setViewExerciseModalIsOpen} /> }  
            {user.profile !== 0 ? <div className="userDashboardContainer">
                <Navbar />
                     <div className="grid">
                        <div className="rowOneProfile">
                          <div style={{display: "flex", justifyContent: "space-around", padding: "0px 30%"}}>
                            <div className='gridHeader'>
                              <h3 style={{paddingBottom: "1rem"}}>Profile</h3>
                              {profile &&
                                <div>
                                    <p><b>First name:</b> {user.firstName}</p>
                                    <p><b>Last name:</b> {user.lastName}</p>
                                    <p><b>Weight:</b> {profile.weight} kg</p>
                                    <p><b>Height:</b> {profile.height} cm</p>
                                    {profile.medicalConditions && <p>Medical: {profile.medicalConditions}</p>}
                                    {profile.disabilities && <p>Disabilities: {profile.disabilities}</p>}
                                    <button className="btnPrimary" onClick={() => setProfileModalIsOpen(true)}>Edit Profile</button>
                                    <Modal ariaHideApp={false} isOpen={profileModalIsOpen} onRequestClose={() => setProfileModalIsOpen(false)}>
                                    <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setProfileModalIsOpen(false)} />
                                    <UpdateProfile profile={profile} setProfile={setProfile}/>                            
                                    </Modal>
                                </div> 
                              }
                              
                            </div>
                            <div className='gridContent'>
                              <div className='iconContainer' style={{marginTop: "auto"}}>
                                <FontAwesomeIcon icon={faUser} size='4x' style={{color: "#fff", marginTop: "1rem"}} />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="rowTwoProfile">
                          <div>
                            <h3 className='gridHeader'>Goals</h3>
                            <button className="btnPrimary" onClick={() => setAddGoalModalIsOpen(true)}>Add Goal</button>
                            <Modal ariaHideApp={false} isOpen={addGoalModalIsOpen} onRequestClose={() => setAddGoalModalIsOpen(false)}>
                                    <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setAddGoalModalIsOpen(false)} />
                                    <CreateGoal />                            
                                    </Modal>
                                {goals.length > 0 ? <div>{goals.map(goal => {
                                  return <div style={{display: "flex", justifyContent: "space-between"}} key={goal.id + Date.now()}>
                                    <GoalItem goal={goal} removeGoal={removeGoal}/>
                                    <div style={{width: "30%", marginTop: "1rem"}}>
                                  <FontAwesomeIcon className='deleteIcon' icon={faXmark} size='2x' style={{marginTop: "0rem", cursor: "pointer", paddingRight: "2.5rem"}} onClick={() => dispatch(deleteGoals(goal.id))} />
                                  <FontAwesomeIcon className='editIcon' icon={faPenToSquare} size='2x' style={{marginTop: "0rem", cursor: "pointer", paddingRight: "2.5rem"}} onClick={() => handleGoalEdit(goal)} />
                                  <FontAwesomeIcon className='viewIcon' icon={faCheck} size='2x' style={{marginTop: "0rem", cursor: "pointer", paddingRight: "2.5rem"}} onClick={() => setProfileModalIsOpen(false)} />
                                  </div>
                                    </div>
                                })}</div> : <h3>No Goals available</h3>}

                                <Modal ariaHideApp={false} isOpen={goalModalIsOpen} onRequestClose={() => setGoalModalIsOpen(false)}>
                                    <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setGoalModalIsOpen(false)} />
                                    <UpdateGoal goal={selectedGoal} />                            
                                    </Modal>
                                <Modal ariaHideApp={false} isOpen={viewProgramModalIsOpen} onRequestClose={() => setViewProgramModalIsOpen(false)}>
                                    <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setViewProgramModalIsOpen(false)} />
                                    <ProgramInfoView />                            
                                    </Modal>
                                <Modal ariaHideApp={false} isOpen={viewWorkoutModalIsOpen} onRequestClose={() => setViewWorkoutModalIsOpen(false)}>
                                    <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setViewWorkoutModalIsOpen(false)} />
                                    <WorkoutInfoView />                            
                                    </Modal>
                                <Modal ariaHideApp={false} isOpen={viewExerciseModalIsOpen} onRequestClose={() => setViewExerciseModalIsOpen(false)}>
                                    <FontAwesomeIcon icon={faXmark} size='2x' style={{color: "#fff", marginTop: "0rem", position: "absolute", right: "20", cursor: "pointer"}} onClick={() => setViewExerciseModalIsOpen(false)} />
                                    <ExerciseInfoView />                            
                                    </Modal>
                            </div>
                        </div>
                </div> 
            </div> : <CreateProfile />}
        </>
    )
}

export default UserDashboard