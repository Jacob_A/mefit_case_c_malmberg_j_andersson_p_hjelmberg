import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ROLE } from '../../Constants/roles';
import keycloak from '../../keycloak';
import { addUser, getUserById, unset } from '../../Slices/userSlice';
import  './login.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'



function LoginSignup() {

    const navigate = useNavigate()
    const { user, userError } = useSelector(state => state.user)
    const dispatch = useDispatch()
    useEffect(() => {
        if(keycloak.authenticated){
            const uid = keycloak.subject
            if(!user){
                dispatch(getUserById(uid))
            }
            if(userError === 'rejected'){
                const isContributor = keycloak.tokenParsed.ROLES.some(role => role === ROLE.CONTRIBUTOR)
                const isAdmin = keycloak.tokenParsed.ROLES.some(role => role === ROLE.ADMIN)
                const body = {
                    id: keycloak.tokenParsed.sub,
                    firstName: keycloak.tokenParsed.given_name,
                    lastName: keycloak.tokenParsed.family_name,
                    contributor: isContributor,
                    admin: isAdmin,
                    profile: null
                }
            dispatch(addUser(body))
        }
        }
    },[userError])

    useEffect(() => {
        if(keycloak.hasRealmRole(ROLE.ADMIN) && user){
            navigate("/admin")
        }
        else if(keycloak.hasRealmRole(ROLE.USER) && user){
            navigate("/profile")
        }
    })

    function handleLogin(){
        dispatch(unset()) //To clear local storage as it wont be needed at login
        keycloak.login();
    }

    function handleLogout() {
        dispatch(unset()) //Clear local storage on log out
        keycloak.logout();
    }
    
    if(!keycloak.authenticated) {
        return (
            <>
                <button className='loginBtn' onClick={handleLogin}>Log in / Sign up <FontAwesomeIcon icon={faArrowRight} size='1x' style={{color: "#fff", marginLeft: "1rem"}} /></button>
            </>
        )
    }
    if(keycloak.authenticated) {
        return (
            <>
                <button onClick={handleLogout}>Log out</button>
                <button onClick={() => console.log(keycloak.tokenParsed)}>Log kc info</button>
                <button onClick={() => console.log(keycloak.token)}>Log token</button>
                <button onClick={() => console.log(user)}>Client check</button>
                <button onClick={() => navigate("/profile")}>profile</button>
                <button onClick={() => navigate("/admin")}>admin</button>
            </>
        )
    }
}

export default LoginSignup