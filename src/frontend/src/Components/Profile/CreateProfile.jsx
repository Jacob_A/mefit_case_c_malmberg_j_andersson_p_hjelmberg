import { useEffect } from "react"
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { addAddress } from "../../api/addressApi"
import { addProfile } from "../../api/profileApi"
import { updateUserById } from "../../Slices/userSlice"

const CreateProfile = () => {

    const { register, handleSubmit, reset } = useForm()
    const { user } = useSelector(state => state.user)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    useEffect(() => {
        if(user.profile){
            navigate("/profile")
        }
    },[user.profile])

    const onProfileSubmit = async (formData) => {

        const addressBody = {
            addressLine1: formData.addr1,
            addressLine2: formData.addr2,
            addressLine3: formData.addr3,
            postalCode: formData.postal,
            city: formData.city,
            country: formData.country,

        }
        const addressResponse = await addAddress(addressBody)
        //Check for error instead
        if(addressResponse[0]){
            //Location responds with address, and id always starts at index 8
            const addressId = addressResponse[0].substring(8)
            const profileBody = {
                user: user.id,
                height: formData.height,
                weight: formData.weight,
                medicalConditions: formData.medical,
                disabilities: formData.disabilities,
                address: parseInt(addressId)
            }

            const profileResponse = await addProfile(profileBody)
            if(profileResponse[0]){
                const profileId = profileResponse[0].substring(8)
                const prevUser = {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    contributor: user.contributor,
                    admin: user.admin,
                    profile: parseInt(profileId)
                }
                dispatch(updateUserById([user.id, prevUser]))
                reset()
            }
        }
    }

    return(
        <div style={{background: "#212121"}}>
            <h3>Profile Creation</h3>
            <form className="d-flex flex-column" onSubmit={handleSubmit(onProfileSubmit)}>
                <label><h3>Height</h3></label>
                <input {...register("height", {required: true})}></input>
                <label><h3>Weight</h3></label>
                <input {...register("weight", {required: true})}></input>
                <label><h3>Medical Conditions</h3></label>
                <input {...register("medical")}></input>
                <label><h3>Disabilities</h3></label>
                <input {...register("disabilities")}></input>
                <label><h3>Address Line 1</h3></label>
                <input {...register("addr1", {required: true})}></input>
                <label><h3>Address Line 2</h3></label>
                <input {...register("addr2")}></input>
                <label><h3>Address Line 3</h3></label>
                <input {...register("addr3")}></input>
                <label><h3>Postal Code</h3></label>
                <input {...register("postal", {required: true})}></input>
                <label><h3>City</h3></label>
                <input {...register("city", {required: true})}></input>
                <label><h3>Country</h3></label>
                <input {...register("country", {required: true})}></input>
                <button className="mt-4" type="submit">Submit</button>
            </form>
        </div>
    )
}

export default CreateProfile