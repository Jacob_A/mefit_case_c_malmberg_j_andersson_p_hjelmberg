import { useState } from "react"
import { useEffect } from "react"
import { Container } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { useSelector } from "react-redux"
import { findAddressById, updateAddress } from "../../api/addressApi"
import { updateProfile } from "../../api/profileApi"
import './style.css'

const UpdateProfile = ({ profile, setProfile }) => {

    const { register, handleSubmit, reset } = useForm()
    const { user } = useSelector(state => state.user)
    const [ currentProfile, setCurrentProfile ] = useState(profile) 
    const [ currentAddress, setCurrentAddress ] = useState(null) 

    useEffect(() => {
        const getAddress = async () => {
          const fetchedAddress = await findAddressById(profile.address)
          if(fetchedAddress[0]){
            console.log(fetchedAddress[0])
            setCurrentAddress(fetchedAddress[0])
          }
        }
        getAddress()
      },[])



    const onProfileSubmit = async (formData) => {


        const addressBody = {
            id: currentAddress.id,
            addressLine1: formData.addressLine1,
            addressLine2: formData.addressLine2,
            addressLine3: formData.addressLine3,
            postalCode: formData.postalCode,
            city: formData.city,
            country: formData.country,
            profiles: currentAddress.profiles
        }
        const addressResponse = await updateAddress(addressBody, currentAddress.id)
        if(addressResponse[0]){ //Successful response
            //Location responds with address and id always starts at index 8
            const profileBody = {
                id: currentProfile.id,
                user: user.id,
                height: formData.height,
                weight: formData.weight,
                medicalConditions: formData.medicalConditions,
                disabilities: formData.disabilities,
                address: currentProfile.id
            }

            const profileResponse = await updateProfile(profileBody, currentProfile.id)
            if(profileResponse[0]){
                setProfile(profileResponse[0])
            }
            reset()
        }
    }

    const onFieldChangeProfile = (e) => {
        //Spread current obj, add new value to object
        setCurrentProfile({...currentProfile, [e.target.name]: e.target.value})
    }

    const onFieldChangeAddress = (e) => {
        //Spread current obj, add new value to object
        setCurrentAddress({...currentAddress, [e.target.name]: e.target.value})
    }

    return(
        <Container>         
            <h3 style={{textAlign: "center", paddingTop: "2rem"}}>Profile Creation</h3>
            {currentProfile && currentAddress &&  
            <form className="d-flex flex-column" style={{width: "70%", marginLeft: "auto", marginRight: "auto"}} onSubmit={handleSubmit(onProfileSubmit)}>
                <label><h5>Heigt</h5></label>
                <input className="marginInput" {...register("height", {required: true})} value={currentProfile.height} onChange={onFieldChangeProfile}></input>
                <label><h5>Weight</h5></label>
                <input className="marginInput" {...register("weight", {required: true})} value={currentProfile.weight} onChange={onFieldChangeProfile}></input>
                <label><h5>Medical Conditions</h5></label>
                <input className="marginInput" {...register("medicalConditions")} value={currentProfile.medicalConditions ? currentProfile.medicalConditions : ""} onChange={onFieldChangeProfile}></input>
                <label><h5>Disabilities</h5></label>
                <input className="marginInput" {...register("disabilities")} value={currentProfile.disabilities ? currentProfile.disabilities : ""} onChange={onFieldChangeProfile}></input>
                <label><h5>Address Line 1</h5></label>
                <input className="marginInput" {...register("addressLine1", {required: true})} value={currentAddress.addressLine1} onChange={onFieldChangeAddress}></input>
                <label><h5>Address Line 2</h5></label>
                <input className="marginInput" {...register("addressLine2")} value={currentAddress.addressLine2 ? currentAddress.addressLine2 : ""} onChange={onFieldChangeAddress}></input>
                <label><h5>Address Line 3</h5></label>
                <input className="marginInput" {...register("addressLine3")} value={currentAddress.addressLine3 ? currentAddress.addressLine3 : ""} onChange={onFieldChangeAddress}></input>
                <label><h5>Postal Code</h5></label>
                <input className="marginInput" {...register("postalCode", {required: true})} value={currentAddress.postalCode} onChange={onFieldChangeAddress}></input>
                <label><h5>City</h5></label>
                <input className="marginInput" {...register("city", {required: true})} value={currentAddress.city} onChange={onFieldChangeAddress}></input>
                <label><h5>Country</h5></label>
                <input className="marginInput" {...register("country", {required: true})} value={currentAddress.country} onChange={onFieldChangeAddress}></input>
                <button className="btnPrimary" type="submit">Submit</button>
            </form> }
        </Container>
    )
}

export default UpdateProfile