import './App.css';
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import UserDashboard from './Components/Dashboards/UserDashboard';
import AdminDashboard from './Components/Dashboards/AdminDashboard'
import UserRoute from './routes/UserRoutes';
import CreateGoal from './Components/Goal/CreateGoal';
import CreateWorkout from './Components/workout/CreateWorkout'
import CreateExercise from './Components/exercise/CreateExercise';
import LandingPage from './Views/LandingPage'
import CreateProgram from './Components/Program/CreateProgram';
import AdminRoute from './routes/AdminRoute'; 
import UpdateExercise from './Components/exercise/UpdateExercise';
import UpdateGoal from './Components/Goal/UpdateGoal';
import UpdateProfile from './Components/Profile/UpdateProfile';
import { Modal } from 'react-bootstrap';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Routes>
        <Route path='/admin' element={<AdminRoute><AdminDashboard/></AdminRoute>}/>
        <Route path='/goal' element={<UserRoute><CreateGoal/></UserRoute>}/>
        <Route path='/createworkout' element={<AdminRoute><CreateWorkout/></AdminRoute>} />
        <Route path='/createexercise' element={<AdminRoute><CreateExercise/></AdminRoute>}/>
        <Route path='/' element={<LandingPage/>}/>
        <Route path='/profile' element={<UserRoute><UserDashboard/></UserRoute>}/>
        <Route path='/createprogram' element={<AdminRoute><CreateProgram /></AdminRoute>} />
      </Routes>
    </div>
    </BrowserRouter>

  );
}

export default App;
