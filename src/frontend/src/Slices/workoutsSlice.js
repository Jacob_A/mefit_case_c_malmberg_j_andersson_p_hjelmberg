import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { addWorkout, deleteWorkout, fetchAllWorkouts, updateWorkout } from "../api/workoutApi"

const initialState = {
    workouts: [],
}


export const workoutsSlice = createSlice({
    name: 'workouts',
    initialState,
    reducers: {
        add: (state, action) => {
            state.workouts = [...state.workouts, action.payload]
        },
    },
    extraReducers: (builder) => {
        builder.addCase(getWorkouts.fulfilled, (state, action) => {
            state.workouts = action.payload
        }).addCase(getWorkouts.rejected, (state, action) => {
            
        }).addCase(updateWorkouts.fulfilled, (state, action) => {
            state.workouts = state.workouts.map(exercise => {
                if(exercise.id === action.payload.id) return action.payload
                else return exercise
            })
        }).addCase(updateWorkouts.rejected, (state, action) => {

        }).addCase(deleteWorkouts.fulfilled, (state, action) => {
            state.workouts = state.workouts.filter(workout => workout.id !== action.payload) 
        })
    }
})


export const getWorkouts = createAsyncThunk('workouts/', async () => {
    const data = await fetchAllWorkouts()
    return data[0]
})

export const updateWorkouts = createAsyncThunk('updateWorkouts', async (obj) => {
    const data = await updateWorkout(obj[0], obj[1])
    return data[0]
})

export const deleteWorkouts = createAsyncThunk('deleteWorkouts', async (id) => {
    const data = await deleteWorkout(id)
    if(data[0]) return id
})

export const addWorkouts = createAsyncThunk('addWorkouts', async (body) => {
    const data = await addWorkout(body)
    if(data[0]) return data[0]
})

export const { add } = workoutsSlice.actions

export default workoutsSlice.reducer