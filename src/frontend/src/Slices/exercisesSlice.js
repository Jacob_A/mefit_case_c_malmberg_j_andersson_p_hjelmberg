import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { deleteExercise, fetchAllExercises, updateExercise } from "../api/exerciseApi"

const initialState = {
    exercises: [],
}


export const exercisesSlice = createSlice({
    name: 'exercises',
    initialState,
    reducers: {
        add: (state, action) => {
            state.exercises = [...state.exercises, action.payload]
        },
        remove: (state, action) => {

        }
    },
    extraReducers: (builder) => {
        builder.addCase(getExercises.fulfilled, (state, action) => {
            state.exercises = action.payload
        }).addCase(getExercises.rejected, (state, action) => {
            
        }).addCase(updateExercises.fulfilled, (state, action) => {
            state.exercises = state.exercises.map(exercise => {
                if(exercise.id === action.payload.id) return action.payload
                else return exercise
            })
        }).addCase(updateExercises.rejected, (state, action) => {

        }).addCase(deleteExercises.fulfilled, (state, action) => {
            state.exercises = state.exercises.filter(exercise => exercise.id !== action.payload)
        })     
    }
})


export const getExercises = createAsyncThunk('exercises/', async () => {
    const data = await fetchAllExercises()
    return data[0]
})

export const updateExercises = createAsyncThunk('updateExercises', async (obj) => {
    const data = await updateExercise(obj[0], obj[1])
    return data[0]
})

export const deleteExercises = createAsyncThunk('deleteExercises', async (id) => {
    const data = await deleteExercise(id)
    if(data[0]) return id
})

export const { add, remove } = exercisesSlice.actions

export default exercisesSlice.reducer