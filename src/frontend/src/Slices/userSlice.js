import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { fetchUserById, postUser, updateUser } from "../api/userApi"
import { STORAGE_KEY_USER } from "../Constants/storageKeys"
import { storageDelete, storageRead, storageSave } from "../Utils/userStorage"

const initialState = {
    user: storageRead(STORAGE_KEY_USER),
    userError: null
}


export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        unset: (state) => {
            state.user = null
            storageDelete(STORAGE_KEY_USER)
        },
        updateUserProfile: (state, action) => {
            state.user = {...state.user, profile: action.payload}
        },
        update: (state, action) => {

        }
    },
    extraReducers: (builder) => {
        builder.addCase(getUserById.fulfilled, (state, action) => {
            state.user = action.payload
            storageSave(STORAGE_KEY_USER, action.payload)
        }).addCase(addUser.fulfilled, (state) => {
            state.userError = null
        }).addCase(getUserById.rejected, (state, action) => {
            state.userError = action.payload
        }).addCase(updateUserById.fulfilled, (state, action) => {
            state.user = action.payload
            storageSave(STORAGE_KEY_USER, action.payload)
        })
        
    }
})


export const getUserById = createAsyncThunk('users/', async (id, {rejectWithValue}) => {
    const data = await fetchUserById(id)
    //Checks if the return is a string (error.message)
    if(typeof(data) === "string"){
        return rejectWithValue("rejected")
    }
    return data
})

//Updates a user and if it is successfully done sets the user in state
export const updateUserById = createAsyncThunk('updateUsers/', async (values) => {
    const data = await updateUser(values[1], values[0])
    return data[0]
})

export const addUser = createAsyncThunk('addUser', async (body) => {
    return await postUser(body)
})

export const { unset, updateUserProfile } = userSlice.actions

export default userSlice.reducer