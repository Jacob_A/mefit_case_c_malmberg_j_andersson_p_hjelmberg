import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { deleteProgram, fetchAllPrograms, updateProgram } from "../api/programApi"

const initialState = {
    programs: [],
}


export const programsSlice = createSlice({
    name: 'programs',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getPrograms.fulfilled, (state, action) => {
            state.programs = action.payload
        }).addCase(getPrograms.rejected, (state, action) => {
            
        }).addCase(updatePrograms.fulfilled, (state, action) => {
            state.programs = state.programs.map(program => {
                if(program.id === action.payload.id) return action.payload
                else return program
            })
        }).addCase(updatePrograms.rejected, (state, action) => {

        }).addCase(deletePrograms.fulfilled, (state, action) => {
            state.programs = state.programs.filter(program => program.id !== action.payload)
        })     
    }
})


export const getPrograms = createAsyncThunk('programs/', async () => {
    const data = await fetchAllPrograms()
    return data[0]
})

export const updatePrograms = createAsyncThunk('updatePrograms', async (obj) => {
    const data = await updateProgram(obj[0], obj[1])
    return data[0]
})

export const deletePrograms = createAsyncThunk('deletePrograms', async (id) => {
    const data = await deleteProgram(id)
    if(data[0]) return id
})

export const {} = programsSlice.actions

export default programsSlice.reducer