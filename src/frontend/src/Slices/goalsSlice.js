import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { deleteGoal, fetchAllGoals, updateGoal } from "../api/goalApi"

const initialState = {
    goals: [],
}


export const goalsSlice = createSlice({
    name: 'goals',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getGoals.fulfilled, (state, action) => {
            //Only wanna set the goals that belong to this profile in state
            state.goals = action.payload[0].filter(goal => goal.profile === action.payload[1])
        }).addCase(getGoals.rejected, (state, action) => {
            
        }).addCase(updateGoals.fulfilled, (state, action) => {
            state.goals = state.goals.map(goal => {
                if(goal.id === action.payload.id) return action.payload
                else return goal
            })
        }).addCase(updateGoals.rejected, (state, action) => {

        }).addCase(deleteGoals.fulfilled, (state, action) => {
            state.goals = state.goals.filter(goal => goal.id !== action.payload)
        })     
    }
})


export const getGoals = createAsyncThunk('goals/', async (profileId) => {
    const data = await fetchAllGoals()
    return [data[0], profileId]
})

export const updateGoals = createAsyncThunk('updateGoals', async (obj) => {
    const data = await updateGoal(obj[0], obj[1])
    return data[0]
})

export const deleteGoals = createAsyncThunk('deleteGoals', async (id) => {
    const data = await deleteGoal(id)
    if(data[0]) return id
})

export const { add, remove } = goalsSlice.actions

export default goalsSlice.reducer