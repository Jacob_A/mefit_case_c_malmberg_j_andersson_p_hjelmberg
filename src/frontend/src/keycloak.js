import Keycloak from 'keycloak-js'


//Import and create a new KeyCloack Object
//Pass in the keycloak.json from public
const keycloak = new Keycloak("keycloak.json")



export const initialize = () => {
    const config = {
        onLoad: "check-sso",
        checkLoginIframe: false,
    }
    return keycloak.init(config)
}

export default keycloak